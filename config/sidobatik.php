<?php

return [
	'number_prefix' => env('SIDOBATIK_NUMBER_PREFIX', 'TRX'),

	'customer_unit_price' => 150000, // in IDR
	'reseller_margin' => 30000, // in IDR
	'design_claim_price' => 150000, // in IDR
	'auto_claim_design' => 15, // in qty
	'product_width' => 220, // in cm
	'product_height' => 115, // in cm
	'product_weight' => 280, // in gr

	'warehouse_province' => '11_jawa-timur', // {rajaongkir_id}_{rajaongkir_slug}
	'warehouse_city' => '256_kota-malang', // {rajaongkir_id}_{rajaongkir_slug}

	'min_design_revise' => 3,
	'invoice_due_days' => 7,

	'feedback_google_form_link' => '#', // google form link
];