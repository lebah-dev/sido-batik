<?php

namespace App\DataTables;

use App\Models\Order;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class OrderDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('status', 'components.orders.status-badge')
            ->addColumn('reseller', function (Order $order) {
                return $order->reseller()
                    ? '<a href="' . route('user-managements.show', $order->reseller) . '">' . $order->reseller->name . '</a>'
                    : '-';
            })
            ->editColumn('created_at', function (Order $order) {
                return '<span title="'.$order->created_at->format('Y-m-d H:i').'">'.$order->created_at->diffForHumans().'</span>';
            })
            ->addColumn('action', 'components.orders.action')
            ->rawColumns(['status', 'reseller', 'created_at', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $model)
    {
        return $model->newQuery()
            ->when(!Auth::user()->isAdmin(), function (Builder $query) {
                $query->where('reseller_id', Auth::id());
            })
            ->when(Request::has('status'), function (Builder $query) {
                $query->where('status', Request::get('status'));
            })
            ->when(Request::has('reseller'), function (Builder $query) {
                $query->where('reseller_id', Request::get('reseller'));
            })
            ->when(Request::has('claimed'), function (Builder $query) {
                if (Request::get('claimed')) {
                    $query->whereNotNull('reseller_claimed_at');
                } else {
                    $query->whereNull('reseller_claimed_at');
                }
            });
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('order-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(2)
                    ->buttons(
                        Button::make('create')->text('<i class="fas fa-plus"></i> Tambah'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('action')
                ->title('Aksi')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
            Column::make('status')->title('Status'),
            Column::make('number')->title('Nomor'),
            Column::make('reseller')->title('Reseller'),
            Column::make('customer')->title('Customer'),
            Column::make('created_at')->title('Dibuat pada'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Order_' . date('YmdHis');
    }
}
