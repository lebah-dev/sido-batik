<?php

namespace App\Http\Requests\Orders;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reseller_id' => 'required|exists:users,id',
            'customer' => 'required',
            'address' => 'required',
            'wa_number' => 'required',
            'email' => 'required',
            'request_description' => 'required',
        ];
    }
}
