<?php

namespace App\Http\Requests\Orders;

use Illuminate\Foundation\Http\FormRequest;

class SaveShippingData extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'receiver_name' => 'required',
            'receiver_phone' => 'required',
            'shipping_province' => 'required',
            'shipping_city' => 'required',
            'shipping_address' => 'required',
            'courier_package' => 'required',  
        ];
    }
}
