<?php

namespace App\Http\Requests\Orders;

use App\Models\ResellerProfile;
use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        if ($this->has('code_reference')) {
            $profile = ResellerProfile::query()
                ->where('code_reference', $this->code_reference)
                ->first();

            $this->merge([
                'reseller_id' => $profile ? $profile->id : null
            ]);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reseller_id' => 'required|exists:users,id',
            'customer' => 'required',
            'address' => 'required',
            'wa_number' => 'required',
            'email' => 'required',
            'request_description' => 'required',
            'image' => 'nullable|image|max:2000',
            'image_description' => 'nullable',
        ];
    }
}
