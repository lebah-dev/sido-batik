<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        if ($this->routeIs('profile.update-profile')) {
            $this->merge([
                'user' => Auth::user(),
            ]);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required',
            'identity_number' => 'required|unique:reseller_profiles,identity_number,'.$this->user->id,
            'wa_number' => 'required|unique:reseller_profiles,wa_number,'.$this->user->id,
            'facebook_account' => 'required|unique:reseller_profiles,facebook_account,'.$this->user->id,
            'portrait_photo' => 'nullable|image|max:2000',
            'identity_scan' => 'nullable|image|max:2000',
        ];
    }
}
