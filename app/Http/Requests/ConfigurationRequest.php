<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ConfigurationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reseller_margin' => 'required',
            
            'warehouse_province' => 'required', // {rajaongkir_id}_{rajaongkir_slug}
            'warehouse_city' => 'required', // {rajaongkir_id}_{rajaongkir_slug}

            'min_design_revise' => 'required',
            'invoice_due_days' => 'required',

            'feedback_google_form_link' => 'required', // google form link
        ];
    }
}
