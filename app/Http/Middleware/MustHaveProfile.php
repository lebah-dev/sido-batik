<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class MustHaveProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /**
         * @var User
         */
        if ($user = Auth::user()) {
            if ($user->role != User::ROLE_ADMIN) {
                if ($user->email_verified_at == null) {
                    return Redirect::route('profile')->with('info', 'Please verify your account by click the link we send to your email address.');
                }
    
                if ($user->profile == null || $user->profile->verified_at == null) {
                    return Redirect::route('profile')->with('info', 'Please fill the form below so we can verified your identity. If done, please wait about 1 to 24 hour for us to verify your profile.');
                }
            }
        }

        return $next($request);
    }
}
