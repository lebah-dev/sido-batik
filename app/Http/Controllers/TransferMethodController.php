<?php

namespace App\Http\Controllers;

use App\Models\TransferMethod;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

class TransferMethodController extends Controller
{
    /**
     * @return RedirectResponse
     */
    public function store()
    {
        TransferMethod::query()->create(Request::validate([
            'bank' => 'required',
            'name' => 'required',
            'number' => 'required',
        ]));

        return Redirect::back();
    }

    /**
     * @param TransferMethod $bank
     * @return RedirectResponse
     */
    public function delete(TransferMethod $bank)
    {
        $bank->delete();
        return Redirect::back();
    }
}
