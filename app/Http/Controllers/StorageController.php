<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;

class StorageController extends Controller
{
	/**
	 * Handle get storage
	 * 
	 * @param string $path encrypted
	 * @return string
	 */
	public function get($path)
	{
		return Storage::get(decrypt($path));
	}

	/**
	 * Handle download storage
	 * 
	 * @param string $path encrypted
	 * @return string
	 */
	public function download($path)
	{
		return Storage::download(decrypt($path));
	}
}