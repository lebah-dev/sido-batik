<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class TransferMarginController extends Controller
{
	/**
	 * @return View
	 */
	public function index()
	{
		return view('transfer-margin.index', [
			'resellers' => User::query()->role(User::ROLE_RESELLER)->get()
		]);
	}

	/**
	 * @return View
	 */
	public function reseller()
	{
		return view('transfer-margin.reseller', [
			'orders' => Auth::user()->orders()->finished()->orderBy('created_at', 'desc')->get(),
		]);
	}
}