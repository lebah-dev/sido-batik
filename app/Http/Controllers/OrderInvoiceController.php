<?php

namespace App\Http\Controllers;

use App\Http\Requests\Orders\MarkAsPaidRequest;
use App\Models\Order;
use App\Models\OrderInvoice;
use App\Models\TransferMethod;
use App\Services\Orders\OrderInvoiceService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;

class OrderInvoiceController extends Controller
{
	/**
	 * @param Order $order
	 * @return RedirectResponse
	 */
	public function create(Order $order, OrderInvoiceService $service)
	{
		Request::validate(['type' => 'required']);
		$service->createInvoice($order, Request::get('type'));
		return Redirect::route('order-managements.show', $order);
	}

	/**
	 * @param OrderInvoice $invoice
	 * @return Illuminate\Contracts\View\View
	 */
	public function show(OrderInvoice $invoice)
	{
		return View::make(Auth::check() ? 'order-invoice.show' : 'order-invoice.customer', [
			'invoice' => $invoice,
			'banks' => TransferMethod::query()->latest()->limit(3)->get(),
		]);
	}

	/**
	 * @param OrderInvoice $invoice
	 * @return Illuminate\Contracts\View\View
	 */
	public function print(OrderInvoice $invoice)
	{
		return View::make('order-invoice.print', [
			'invoice' => $invoice,
			'banks' => TransferMethod::query()->latest()->limit(3)->get(),
		]);
	}

	/**
	 * @param OrderInvoice $invoice
	 * @return Illuminate\Contracts\View\View
	 */
	public function paid(MarkAsPaidRequest $request, OrderInvoice $invoice, OrderInvoiceService $service)
	{
		$service->paid($invoice, $request);
		return Redirect::back();
	}

	/**
	 * @param OrderInvoice $invoice
	 * @param OrderInvoiceService $service
	 * 
	 * @return RedirectResponse
	 */
	public function reset(OrderInvoice $invoice, OrderInvoiceService $service)
	{
		$service->reset($invoice);
		return Redirect::back();
	}
}