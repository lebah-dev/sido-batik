<?php

namespace App\Http\Controllers;

use App\Models\Pricing;
use Illuminate\Contracts\View\View as ViewView;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;

class PricingController extends Controller
{
    /**
     * @return ViewView
     */
    public function index()
    {
        return View::make('pricings.index', [
            'pricings' => Pricing::query()
                ->orderBy('width')
                ->orderBy('length')
                ->orderBy('min_qty')
                ->get(),
        ]);
    }

    /**
     * @return ViewView
     */
    public function create()
    {
        return View::make('pricings.create');
    }

    /**
     * @return RedirectResponse
     */
    public function store()
    {
        Pricing::query()
            ->updateOrCreate(Request::validate([
                'width' => 'required',
                'length' => 'required',
                'weight' => 'required',
                'min_qty' => 'required',
            ]), Request::validate([
                'unit_price' => 'required',
            ]));

        return Redirect::route('pricings.index');
    }

    /**
     * @param Pricing $pricing
     * @return ViewView
     */
    public function edit(Pricing $pricing)
    {
        return View::make('pricings.create', [
            'pricing' => $pricing
        ]);
    }

    /**
     * @param Pricing $pricing
     * @return RedirectResponse
     */
    public function destroy(Pricing $pricing)
    {
        $pricing->delete();
        return back();
    }
}
