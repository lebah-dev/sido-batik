<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Contracts\View\View as ViewView;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;

class TrackOrderController extends Controller
{
	/**
	 * @return ViewView
	 */
	public function index()
	{
		$data = null;
		if (Request::query('order') && Request::query('reseller')) {
			$data = Order::query()
				->whereHas('reseller', function (Builder $reseller) {
					$reseller->whereHas('profile', function (Builder $profile) {
						$profile->where('code_reference', Request::query('reseller'));
					});
				})
				->where('number', Request::query('order'))
				->first();
		}

		return View::make('track-order', [
			'order' => $data
		]);
	}
}