<?php

namespace App\Http\Controllers;

use App\Repositories\DashboardRepository;
use Illuminate\Support\Facades\View;

class DashboardController extends Controller
{
	public function index(DashboardRepository $repository)
	{
		return View::make('dashboard', [
			'statistics' => [
				'total_reseller' => $repository->getTotalReseller(),
				'claimable_order' => $repository->getClaimableOrder(),

				'new_order' => $repository->getTotalNewOrder(),
				'ongoing_order' => $repository->getTotalOngoingOrder(),
				'finished_order' => $repository->getTotalFinishedOrder(),

				'belum_dp' => $repository->getTotalPendingDpOrder(),
				'sudah_dp' => $repository->getTotalFinishedDpOrder(),
				'belum_lunas' => $repository->getTotalPendingFpOrder(),
				'sudah_lunas' => $repository->getTotalFinishedFpOrder(),
			],

			'newOrders' => $repository->getNewOrders(),
			'placedOrderGraph' => $repository->getPlacedOrderGraph(), 
		]);
	}
}