<?php

namespace App\Http\Controllers;

use App\Models\MarketingMaterial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class MarketingMaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View::make('marketing-materials.index', [
            'marketingMaterials' => MarketingMaterial::latest()->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('marketing-materials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $marketingMaterial = new MarketingMaterial($request->validate([
            'title' => 'required',
            'image' => 'required',
            'caption' => 'required',
        ]));

        if ($marketingMaterial->image) {
            $marketingMaterial->image_path = $request->file('image')->store('marketing-materials');
        }

        $marketingMaterial->save();
        return redirect()->route('marketing-materials.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  MarketingMaterial $marketingMaterial
     * @return \Illuminate\Http\Response
     */
    public function edit(MarketingMaterial $marketingMaterial)
    {
        return View::make('marketing-materials.edit', [
            'marketingMaterial' => $marketingMaterial
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  MarketingMaterial $marketingMaterial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MarketingMaterial $marketingMaterial)
    {
        $marketingMaterial->fill($request->validate([
            'title' => 'required',
            'caption' => 'required',
        ]));

        if ($request->hasFile('image')) {
            Storage::delete($marketingMaterial->image_path);
            $marketingMaterial->image_path = $request->file('image')->store('marketing-materials');
        }

        $marketingMaterial->save();
        return redirect()->route('marketing-materials.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(MarketingMaterial $marketingMaterial)
    {
        $marketingMaterial->delete();
        return redirect()->route('marketing-materials.index');
    }
}
