<?php

namespace App\Http\Controllers;

use App\Http\Requests\ConfigurationRequest;
use App\Models\TransferMethod;
use App\Services\ConfigurationService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class ConfigurationController extends Controller
{
	/**
	 * @return Illuminate\Contracts\View\View
	 */
	public function index(ConfigurationService $configurationService)
	{
		return View::make('configurations', [
			'config' => $configurationService,
			'transferMethods' => TransferMethod::all(),
		]);
	}

	/**
	 * @param ConfigurationService $configurationService
	 * @return RedirectResponse
	 */
	public function update(ConfigurationRequest $request, ConfigurationService $configurationService)
	{
		$configurationService->update($request);
		return Redirect::route('configurations');
	}

	/**
	 * @param ConfigurationService $configurationService
	 * @return RedirectResponse
	 */
	public function reset(ConfigurationService $configurationService)
	{
		$configurationService->reset();
		return Redirect::route('configurations');
	}
}