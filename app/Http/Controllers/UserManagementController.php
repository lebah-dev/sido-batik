<?php

namespace App\Http\Controllers;

use App\DataTables\UsersDataTable;
use App\Http\Requests\UpdateProfileRequest;
use App\Http\Requests\UserManagement\UpdateBankAccountRequest;
use App\Http\Requests\UserManagement\UpdateUserAccountRequest;
use App\Models\User;
use App\Services\BankAccountService;
use App\Services\ProfileService;
use App\Services\UserManagementService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;

class UserManagementController extends Controller
{
	/**
	 * Handle index request
	 * 
	 * @param UsersDataTable $dataTable
	 * @return mixed
	 */
	public function index(UsersDataTable $dataTable)
	{
		return $dataTable->render('user-managements.index');
	}

	/**
	 * Handle create request
	 * 
	 * @return \Illuminate\Contracts\View\View
	 */
	public function create()
	{
		return View::make('user-managements.create', [
			'roles' => User::roles()
		]);
	}

	/**
	 * Handle store request
	 * 
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		Request::validate([
			'role' => 'required|in:' . implode(',', array_keys(User::roles())),
			'name' => 'required',
			'email' => 'required|email|unique:users,email',
			'password' => 'required|confirmed' 
		]);

		$user = new User(Request::only(['role', 'name', 'email']));
		$user->password = bcrypt(Request::get('password'));
		$user->save();

		return Redirect::route('user-managements.index');
	}

	/**
	 * Handle show request
	 * 
	 * @param User $user
	 * @return \Illuminate\Contracts\View\View
	 */
	public function show(User $user)
	{
		return View::make('user-managements.show', [
			'user' => $user
		]);
	}

	/**
	 * Handle update profile request
	 * 
	 * @param UpdateProfileRequest $request
	 * @param User $user
	 * @param ProfileService $service
	 * @return RedirectResponse
	 */
	public function updateProfile(UpdateProfileRequest $request, User $user, ProfileService $service)
	{
		$service->update($request, $user);
		return Redirect::route('user-managements.show', $user);
	}

	/**
	 * Handle verify profile request
	 * 
	 * @param User $user
	 * @param ProfileService $service
	 * @return RedirectResponse
	 */
	public function verify(User $user, ProfileService $service)
	{
		$service->verify($user);
		return Redirect::route('user-managements.show', $user);
	}

	/**
	 * Handle verify update bank account request
	 * 
	 * @param UpdateBankAccountRequest $request
	 * @param User $user
	 * @param BankAccountService $service
	 * 
	 * @return RedirectResponse
	 */
	public function updateBankAccount(UpdateBankAccountRequest $request, User $user, BankAccountService $service)
	{
		$service->update($user, $request);
		return Redirect::back();
	}

	/**
	 * Handle edit request
	 * 
	 * @param User $user
	 * @return \Illuminate\Contracts\View\View
	 */
	public function edit(User $user)
	{
		return View::make('user-managements.edit', [
			'roles' => User::roles(),
			'user' => $user
		]);
	}

	/**
	 * Handle update request
	 * 
	 * @param UpdateUserAccountRequest $request
	 * @param User $user
	 * @param UserManagementService $service
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(UpdateUserAccountRequest $request, User $user, UserManagementService $service)
	{
		$service->update($request, $user);
		return Redirect::route('user-managements.index');
	}

	/**
	 * Handle delete request
	 * 
	 * @param User $user
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete(User $user)
	{
		if (Auth::id() != $user->id) $user->delete();

		return Redirect::route('user-managements.index');
	}
}