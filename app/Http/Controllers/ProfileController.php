<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateProfileRequest;
use App\Http\Requests\UserManagement\UpdateUserAccountRequest;
use App\Services\ProfileService;
use App\Services\UserManagementService;
use Illuminate\Contracts\View\View as ViewView;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class ProfileController extends Controller
{
	/**
	 * @return ViewView
	 */
	public function index()
	{
		return View::make('profile', [
			'user' => Auth::user()
		]);
	}

	/**
	 * Handle update account
	 * 
	 * @param UpdateUserAccountRequest $request
	 * @param UserManagementService $service
	 * 
	 * @return RedirectResponse
	 */
	public function updateAccount(UpdateUserAccountRequest $request, UserManagementService $serivce)
	{
		$serivce->update($request, Auth::user());
		return Redirect::route('profile');
	}

	/**
	 * Handle update account
	 * 
	 * @param UpdateProfileRequest $request
	 * @param ProfileService $service
	 * 
	 * @return RedirectResponse
	 */
	public function updateProfile(UpdateProfileRequest $request, ProfileService $serivce)
	{
		$serivce->update($request, Auth::user());
		return Redirect::route('profile');
	}
}