<?php

namespace App\Http\Controllers;

use App\Http\Requests\Orders\StoreOrderRequest;
use App\Services\Orders\StoreOrderService;
use Illuminate\Contracts\View\View as ViewView;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class PlaceOrderController extends Controller
{
	/**
	 * @return ViewView
	 */
	public function create()
	{
		return View::make('place-order');
	}

	public function store(StoreOrderRequest $request, StoreOrderService $service)
	{
		$order = $service->store($request);
		return Redirect::route('track-order', ['reseller' => $request->code_reference, 'order' => $order->number]);
	}
}