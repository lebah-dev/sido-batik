<?php

namespace App\Http\Controllers;

use App\DataTables\OrderDataTable;
use App\Http\Requests\Orders\FixOrderRequest;
use App\Http\Requests\Orders\ReviseDesignRequest;
use App\Http\Requests\Orders\SaveShippingData;
use App\Http\Requests\Orders\StoreImageRequest;
use App\Http\Requests\Orders\StoreOrderDesignRequest;
use App\Http\Requests\Orders\StoreOrderRequest;
use App\Http\Requests\Orders\StoreReviseAttachmentRequest;
use App\Http\Requests\Orders\UpdateOrderRequest;
use App\Models\Order;
use App\Models\OrderDesign;
use App\Models\OrderDesignRevision;
use App\Models\OrderImage;
use App\Models\Pricing;
use App\Repositories\UserRepository;
use App\Services\Orders\OrderInvoiceService;
use App\Services\Orders\ProcessDesignService;
use App\Services\Orders\ProcessOrderService;
use App\Services\Orders\StoreOrderDesignService;
use App\Services\Orders\StoreOrderService;
use App\Services\Orders\UpdateOrderService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;

class OrderController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param Order $order
     * @return RedirectResponse
     */
    private function redirect(Order $order)
    {
        return Auth::check() 
            ? Redirect::route('order-managements.show', $order)
            : Redirect::route('track-order', ['reseller' => $order->reseller->profile->code_reference, 'order' => $order->number]);
    }

    /**
     * Handle index request
     * 
     * @return mixed
     */
    public function index(OrderDataTable $dataTable)
    {
        return $dataTable->render('order-managements.index', [
            'statuses' => Order::getAllStatuses()
        ]);
    }

    /**
     * Handle create request
     * 
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        return View::make('order-managements.create', [
            'resellers' => $this->userRepository->getUserResellers()
        ]);
    }

    /**
     * Handle store request
     * 
     * @return RedirectResponse
     */
    public function store(StoreOrderRequest $request, StoreOrderService $service)
    {
        $service->store($request);
        return Redirect::route('order-managements.index');
    }

    /**
     * Handle show request
     * 
     * @param Order $order
     * @return \Illuminate\Contracts\View\View
     */
    public function show(Order $order)
    {
        return View::make('order-managements.show', [
            'order' => $order,
        ]);
    }

    /**
     * Handle edit request
     * 
     * @param Order $order
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Order $order)
    {
        return View::make('order-managements.edit', [
            'order' => $order,
            'resellers' => $this->userRepository->getUserResellers(),
        ]);
    }

    /**
     * Handle update request
     * 
     * @param UpdateOrderRequest $request
     * @param Order $order
     * @param UpdateOrderService $service
     * @return RedirectResponse
     */
    public function update(UpdateOrderRequest $request, Order $order, UpdateOrderService $service)
    {
        $service->update($request, $order);
        return Redirect::route('order-managements.show', $order);
    }

    /**
     * Handle store image request
     * 
     * @param StoreImageRequest $request
     * @param Order $order
     * @param UpdateOrderService $service
     * @return RedirectResponse
     */
    public function storeImage(StoreImageRequest $request, Order $order, UpdateOrderService $service)
    {
        $service->storeImage($request, $order);
        return $this->redirect($order);
    }

    /**
     * Handle delete image request
     * 
     * @param Order $order
     * @param OrderImage $image
     * @param UpdateOrderService $service
     * @return RedirectResponse
     */
    public function deleteImage(Order $order, OrderImage $image, UpdateOrderService $service)
    {
        $service->deleteImage($order, $image);
        return $this->redirect($order);
    }

    /**
     * Handle process order request
     * 
     * @param Order $order
     * @param ProcessOrderService $service
     * @return RedirectResponse
     */
    public function process(Order $order, ProcessOrderService $service)
    {
        $service->process($order);
        return Redirect::route('order-managements.show', $order);
    }

    /**
     * Handle store design request
     * 
     * @param StoreOrderDesignRequest $request
     * @param Order $order
     * @param StoreOrderDesignService $service
     * @return RedirectResponse
     */
    public function storeDesign(StoreOrderDesignRequest $request, Order $order, StoreOrderDesignService $service)
    {
        $service->store($request, $order);
        return Redirect::route('order-managements.show', $order);
    }

    /**
     * Handle revise design request
     * 
     * @param ReviseDesignRequest $request
     * @param Order $order
     * @param OrderDesign $design
     * @param ProcessDesignService $service
     * @return RedirectResponse
     */
    public function reviseDesign(
        ReviseDesignRequest $request,
        Order $order,
        OrderDesign $design,
        ProcessDesignService $service
    ) {
        $service->revise($request, $order, $design);
        return $this->redirect($order);
    }

    /**
     * Handle store revise design attachment request
     * 
     * @param StoreReviseAttachmentRequest $request
     * @param Order $order
     * @param OrderDesign $design
     * @param OrderDesignRevision $revision
     * @param ProcessDesignService $service
     * @return RedirectResponse
     */
    public function storeReviseAttachment(
        StoreReviseAttachmentRequest $request,
        Order $order,
        OrderDesignRevision $revision,
        ProcessDesignService $service
    ) {
        $service->storeRevision($request, $order, $revision);
        return $this->redirect($order);
    }

    /**
     * Handle store revise design attachment request
     * 
     * @param Order $order
     * @param OrderDesign $design
     * @param ProcessDesignService $service
     * @return RedirectResponse
     */
    public function approveDesign(
        Order $order,
        OrderDesign $design,
        ProcessDesignService $service
    ) {
        $service->approve($design);
        return $this->redirect($order);
    }

    /**
     * Handle Fix Order Request
     * 
     * @param FixOrderRequest $request
     * @param Order $order
     * @param ProcessOrderService $service
     * @return RedirectResponse
     */
    public function fixOrder(FixOrderRequest $request, Order $order, ProcessOrderService $service, OrderInvoiceService $invoiceService)
    {
        $service->fixOrder($request, $order);
        $invoiceService->createInvoice($order, 'down-payment');
        return $this->redirect($order);
    }

    /**
     * Handle Save Shipping Data
     * 
     * @param SaveShippingData $request
     * @param Order $order
     * @param ProcessOrderService $service
     * @return RedirectResponse
     */
    public function saveShippingData(SaveShippingData $request, Order $order, ProcessOrderService $service, OrderInvoiceService $invoiceService)
    {
        $service->saveShippingData($request, $order);
        $invoiceService->createInvoice($order, 'final-payment');
        return $this->redirect($order);
    }

    /**
     * Handle Save Shipping Code
     * 
     * @param Order $order
     * @param ProcessOrderService $service
     * @return RedirectResponse
     */
    public function saveShippingCode(Order $order, ProcessOrderService $service)
    {
        Request::validate(['shipping_code' => 'required']);

        $service->saveShippingCode(Request::get('shipping_code'), $order);
        return Redirect::route('order-managements.show', $order);
    }

    /**
     * Handle Claim Margin
     * 
     * @param Order $order
     * @param ProcessOrderService $service
     * @return RedirectResponse
     */
    public function claimMargin(Order $order, ProcessOrderService $service)
    {
        $service->claimMargin($order);
        return Redirect::route('order-managements.show', $order);
    }

    /**
     * Handle upload Delivery Receipt
     * 
     * @param HttpRequest $request
     * @param Order $order
     * @param ProcessOrderService $service
     * 
     * @return RedirectResponse
     */
    public function uploadDeliveryReceipt(HttpRequest $request, Order $order, ProcessOrderService $service)
    {
        $service->storeDeliveryReceipt($order, $request);
        return $this->redirect($order);
    }
}
