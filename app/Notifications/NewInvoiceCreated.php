<?php

namespace App\Notifications;

use App\Models\Order;
use App\Models\OrderInvoice;
use App\Services\ConfigurationService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewInvoiceCreated extends Notification
{
    use Queueable;

    /**
     * @var OrderInvoice
     */
    private $invoice;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(OrderInvoice $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  Order  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  Order  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $resellerCode = $notifiable->reseller->profile->code_reference;
        $route = route('track-order', [
            'reseller' => $resellerCode,
            'order' => $notifiable->number,
        ]);

        return (new MailMessage)
            ->subject('Order Batik : ' . $notifiable->number)
            ->greeting('Halo Kak,')
            ->line('Kami telah menerbitkan invoice untuk order kakak dengan detail sebagai berikut:')
            ->line('Tipe Invoice : ' . $this->invoice->type . ';')
            ->line('Nomor Invoice : ' . $this->invoice->number . ';')
            ->line('Total Pembayaran : ' . $this->invoice->amount . ';')
            ->line('Batas Pembayaran : ' . $this->invoice->due_date->format('d-M-Y H:i') . ';')
            ->line('')
            ->line('Kakak dapat melihat invoice pada halaman track order atau dengan link di bawah ini.')
            ->action('Track Order', $route)
            ->line('Diharap untuk segera melakukan pembayaran pada rekening Sidobatik yang tertera pada invoice kakak. Dan jangan lupa untuk melakukan konfirmasi pembayaran ya kak.')
            ->line('Terimakasih telah memilih kami.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
