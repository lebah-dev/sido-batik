<?php

namespace App\Notifications;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderFinished extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  Order  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  Order  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $resellerCode = $notifiable->reseller->profile->code_reference;
        $route = route('track-order', [
            'reseller' => $resellerCode,
            'order' => $notifiable->number,
        ]);

        return (new MailMessage)
            ->subject('Order Batik : ' . $notifiable->number)
            ->greeting('Halo Kak,')
            ->line('Order kakak dengan detail sebagai berikut:')
            ->line('Nomor Order : ' . $notifiable->number)
            ->line('Reseller Code : ' . $resellerCode)
            ->line('Tanggal : ' . $notifiable->created_at->format('d-M-Y H:i') . ';')
            ->line('')
            ->line('Telah selesai, jangan lupa untuk memberikan kiritik dan saran agar kami dapat selalu meningkatkan kualitas pelayanan kami.')
            ->line('Kakak dapat memberi kritik dan saran pada halaman track order atau dengan link di bawah ini.')
            ->action('Track Order', $route)
            ->line('Terimakasih telah memilih kami.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
