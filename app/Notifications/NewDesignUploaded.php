<?php

namespace App\Notifications;

use App\Models\Order;
use App\Services\ConfigurationService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewDesignUploaded extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  Order  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  Order  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $resellerCode = $notifiable->reseller->profile->code_reference;
        $route = route('track-order', [
            'reseller' => $resellerCode,
            'order' => $notifiable->number,
        ]);

        return (new MailMessage)
            ->subject('Order Batik : ' . $notifiable->number)
            ->greeting('Halo Kak,')
            ->line('Kami sudah mengirimkan desain untuk order kakak dengan detail sebagai berikut:')
            ->line('Nomor Order : ' . $notifiable->number . ';')
            ->line('Reseller Code : ' . $resellerCode . ';')
            ->line('Tanggal : ' . $notifiable->created_at->format('d-M-Y H:i') . ';')
            ->line('')
            ->line('Kakak dapat melihat hasil desain pada halaman track order atau dengan link di bawah ini.')
            ->action('Track Order', $route)
            ->line('Jika ada yang kurang, kakak dapat mengeajukan revisi desain maksimal ' . ConfigurationService::call()->get('min_design_revise') . 'x.')
            ->line('Terimakasih telah memilih kami.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
