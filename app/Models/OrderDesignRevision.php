<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class OrderDesignRevision extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'order_design_revisions';

    /**
     * @var array
     */
    protected $fillable = [
        'order_design_id', 'text'
    ];

    /**
     * @return BelongsTo
     */
    public function orderDesign()
    {
        return $this->belongsTo(OrderDesign::class, 'order_design_id');
    }

    /**
     * @return HasMany
     */
    public function attachments()
    {
        return $this->hasMany(OrderDesignRevisionAttachment::class, 'design_revision_id');
    }

    /**
     * @return HasOne
     */
    public function designRevision()
    {
        return $this->hasOne(OrderDesign::class, 'revision_id');
    }
}
