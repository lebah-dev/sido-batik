<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ResellerProfile extends Model
{
    use HasFactory;

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'full_name',
        'identity_number',
        'wa_number',
        'facebook_account',
        'portrait_photo_path',
        'identity_scan_path',
        'verified_at',
        'code_reference',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'verified_at' => 'datetime',
    ];

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'id');
    }

    /**
     * @return string
     */
    public function getPortraitPhotoAttribute()
    {
        return route('storage-get', encrypt($this->portrait_photo_path));
    }

    /**
     * @return string
     */
    public function getIdentityScanAttribute()
    {
        return route('storage-get', encrypt($this->identity_scan_path));
    }
}
