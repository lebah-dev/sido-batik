<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class OrderDesignRevisionAttachment extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'design_revision_attachments';

    /**
     * @var array
     */
    protected $fillable = [
        'design_revision_id',
        'file_path',
        'description',
    ];

    /**
     * @return BelongsTo
     */
    public function revision()
    {
        return $this->belongsTo(OrderDesignRevision::class, 'design_revision_id');
    }

    /**
     * @return string
     */
    public function getFileAttribute()
    {
        return route('storage-download', encrypt($this->file_path));
    }
}
