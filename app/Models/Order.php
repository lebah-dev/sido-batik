<?php

namespace App\Models;

use App\Services\ConfigurationService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

class Order extends Model
{
    use HasFactory, Notifiable;

    const STATUS_NEW = 'new';
    const STATUS_DESIGN = 'design';
    const STATUS_DOWN_PAYMENT = 'down-payment';
    const STATUS_PRODUCTION = 'production';
    const STATUS_PAINTING = 'painting';
    const STATUS_FINAL_PAYMENT = 'final-payment';
    const STATUS_PACKAGING = 'packaging';
    const STATUS_DELIVER = 'deliver';
    const STATUS_FINISHED = 'finished';

    /**
     * @var string
     */
    protected $table = 'orders';

    /**
     * @var array
     */
    protected $fillable = [
        'number',
        'reseller_id',
        'status',
        'customer',
        'address',
        'wa_number',
        'email',
        'request_description',
        'size',
        'weight',
        'quantity',
        'is_design_claimed',
        'receiver_name',
        'receiver_phone',
        'shipping_province',
        'shipping_city',
        'shipping_address',
        'courier_package',
        'delivery_fee',
        'total_price',
        'notes',
        'shipping_code',
        'reseller_claimed_at',
        'delivery_receipt_path',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'is_design_claimed' => 'boolean',
        'reseller_claimed_at' => 'datetime',
    ];

    /**
     * @return BelongsTo
     */
    public function reseller()
    {
        return $this->belongsTo(User::class, 'reseller_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function images()
    {
        return $this->hasMany(OrderImage::class, 'order_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function designs()
    {
        return $this->hasMany(OrderDesign::class, 'order_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function invoices()
    {
        return $this->hasMany(OrderInvoice::class, 'order_id', 'id');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeFinished(Builder $query)
    {
        return $query->where('status', self::STATUS_FINISHED);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeUnclaimed(Builder $query)
    {
        return $query->whereNull('reseller_claimed_at');
    }

    /**
     * Accessor for $order->shipping_province_name
     * @return string
     */
    public function getShippingProvinceNameAttribute()
    {
        return Str::title(str_replace('-', ' ', explode('_', $this->shipping_province)[1] ?? null));
    }

    /**
     * Accessor for $order->shipping_city_name
     * @return string
     */
    public function getShippingCityNameAttribute()
    {
        return Str::title(str_replace('-', ' ', explode('_', $this->shipping_city)[1] ?? null));
    }

    /**
     * Accessor for $order->reseller_claimable
     * @return double
     */
    public function getResellerClaimableAttribute()
    {
        return $this->quantity * ConfigurationService::call()->get('reseller_margin');
    }

    /**
     * Accessor for $order->delivery_receipt
     * @return string|null
     */
    public function getDeliveryReceiptAttribute()
    {
        return $this->delivery_receipt_path != null 
            ? route('storage-get', encrypt($this->delivery_receipt_path))
            : null;
    }

    /**
     * @return bool
     */
    public function passDesign()
    {
        return in_array($this->status, [
            static::STATUS_DOWN_PAYMENT,
            static::STATUS_PRODUCTION,
            static::STATUS_PAINTING,
            static::STATUS_FINAL_PAYMENT,
            static::STATUS_PACKAGING,
            static::STATUS_DELIVER,
            static::STATUS_FINISHED,
        ]);
    }

    /**
     * @return bool
     */
    public function passProduction()
    {
        return in_array($this->status, [
            static::STATUS_FINAL_PAYMENT,
            static::STATUS_PACKAGING,
            static::STATUS_DELIVER,
            static::STATUS_FINISHED,
        ]);
    }

    /**
     * @return bool
     */
    public function passPayment()
    {
        return in_array($this->status, [
            static::STATUS_PACKAGING,
            static::STATUS_DELIVER,
            static::STATUS_FINISHED,
        ]);
    }

    /**
     * @return OrderInvoice
     */
    public function getPendingDownPaymentInvoice()
    {
        return $this->invoices()
            ->where('type', OrderInvoice::TYPE_DOWN_PAYMENT)
            ->where('status', '!=', OrderInvoice::STATUS_EXPIRED)
            ->first();
    }

    /**
     * @return OrderInvoice
     */
    public function getPendingFinalPaymentInvoice()
    {
        return $this->invoices()
            ->where('type', OrderInvoice::TYPE_FINAL_PAYMENT)
            ->where('status', '!=', OrderInvoice::STATUS_EXPIRED)
            ->first();
    }

    /**
     * Generate New Order Number
     * 
     * @return string
     */
    public static function generateNewNumber()
    {
        return config('sidobatik.number_prefix') . '-' . date('ym') . '-' . date('dH') . '-' . date('i');
    }

    /**
     * Get Order Statuses
     * 
     * @return array
     */
    public static function getAllStatuses()
    {
        return [
            static::STATUS_NEW,
            static::STATUS_DESIGN,
            static::STATUS_DOWN_PAYMENT,
            static::STATUS_PRODUCTION,
            static::STATUS_PAINTING,
            static::STATUS_FINAL_PAYMENT,
            static::STATUS_PACKAGING,
            static::STATUS_DELIVER,
            static::STATUS_FINISHED,
        ];
    }
}
