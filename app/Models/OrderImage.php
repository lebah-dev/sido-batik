<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class OrderImage extends Model
{
    use HasFactory;

    protected $table = 'order_images';
    protected $fillable = [
        'order_id',
        'image_path',
        'description',
    ];

    /**
     * @return BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    /**
     * @return string
     */
    public function getImageAttribute()
    {
        return route('storage-get', encrypt($this->image_path));
    }
}
