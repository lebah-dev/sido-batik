<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pricing extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = [
        'width', 'length', 'weight', 'min_qty', 'unit_price',
    ];
}
