<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class OrderDesign extends Model
{
    use HasFactory;

    const STATUS_REVIEW = 'review';
    const STATUS_APPROVE = 'approve';
    const STATUS_REJECT = 'reject';

    /**
     * @var string
     */
    protected $table = 'order_designs';

    /**
     * @var array
     */
    protected $fillable = [
        'order_id', 'version', 'file_path', 'revision_id', 'status', 'revision_id'
    ];

    /**
     * @return BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    /**
     * @return HasMany
     */
    public function revision()
    {
        return $this->hasOne(OrderDesignRevision::class, 'order_design_id');
    }

    /**
     * @return BelongsTo
     */
    public function fromRevision()
    {
        return $this->belongsTo(OrderDesignRevision::class, 'revision_id');
    }

    /**
     * @param Builder $query
     * @return void
     */
    public function scopeApprove(Builder $query)
    {
        $query->where('status', self::STATUS_APPROVE);
    }

    /**
     * @param Builder $query
     * @return void
     */
    public function scopeReject(Builder $query)
    {
        $query->where('status', self::STATUS_REJECT);
    }

    /**
     * @return string
     */
    public function getFileAttribute()
    {
        return route('storage-download', encrypt($this->file_path));
    }

    /**
     * Get design next version by order id
     * 
     * @param integer $orderId
     * @return integer
     */
    public static function getNextDesignVerision($orderId)
    {
        $lastVersion = static::query()->where('order_id', $orderId)->max('version');

        return $lastVersion ? $lastVersion + 1 : 1;
    }

}
