<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class OrderInvoice extends Model
{
    use HasFactory;

    const TYPE_DOWN_PAYMENT = 'down-payment';
    const TYPE_FINAL_PAYMENT = 'final-payment';

    const STATUS_PENDING = 'pending';
    const STATUS_PAID = 'paid';
    const STATUS_EXPIRED = 'expired';

    /**
     * @var string
     */
    protected $table = 'order_invoices';

    /**
     * @var array
     */
    protected $fillable = [
        'order_id',
        'type',
        'number',
        'amount',
        'due_date',
        'receipt_path',
        'status',
        'paid_at',
        'payment_receipt_path',
        'payment_code',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'due_date' => 'datetime',
        'paid_at' => 'datetime',
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'number';
    }

    /**
     * @return BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    /**
     * @return string|null
     */
    public function getPaymentReceiptAttribute()
    {
        return $this->payment_receipt_path ? route('storage-download', encrypt($this->payment_receipt_path)) : null;
    }

    /**
     * Generate Order Invoice Number
     * 
     * @return string
     */
    public static function generateNumber()
    {
        return 'INV-' . date('ym') . '-' . date('dH') . '-' . date('i');
    }
}
