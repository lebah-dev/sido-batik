<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MarketingMaterial extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'image_path',
        'caption',
    ];

    public function getImageAttribute()
    {
        return route('storage-get', encrypt($this->image_path));
    }
}
