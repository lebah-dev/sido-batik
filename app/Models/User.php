<?php

namespace App\Models;

use App\Services\ConfigurationService;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;
    
    const ROLE_ADMIN = 'admin';
    const ROLE_RESELLER = 'reseller';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role',
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return HasOne
     */
    public function profile()
    {
        return $this->hasOne(ResellerProfile::class, 'id');
    }

    /**
     * @return HasOne
     */
    public function bankAccount()
    {
        return $this->hasOne(BankAccount::class, 'id');
    }

    /**
     * @return HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class, 'reseller_id');
    }

	/**
     * @param Builder $query
     * @param string $role
	 * @return Builder
	 */
	public function scopeRole(Builder $query, string $role)
	{
		return $query->where('role', $role);
    }

    /**
     * Get reseller unclaimed margin
     * 
     * @return double
     */
    public function getUnclaimedMargin()
    {
        $unclaimedOrders = $this->orders()->finished()->unclaimed()->get();

        return $unclaimedOrders->sum(function ($item) {
            return $item->quantity * ConfigurationService::config('reseller_margin');
        });
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->role == self::ROLE_ADMIN;
    }

    /**
	 * Get user available roles
	 * 
	 * @return array
	 */
	public static function roles()
	{
		return [
			static::ROLE_ADMIN => 'Admin',
			static::ROLE_RESELLER => 'Reseller',
		];
	}
}
