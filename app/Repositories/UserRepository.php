<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

class UserRepository
{
	/**
	 * @var User
	 */
	private $user;
	
	/**
	 * Class constructor
	 * 
	 * @param User $user
	 */
	public function __construct(User $user)
	{
		$this->user = $user;
	}

	/**
	 * Get users with role resellers order by name asc
	 * 
	 * @return Collection
	 */
	public function getUserResellers()
	{
		return $this->user->newQuery()
			->role(User::ROLE_RESELLER)
			->orderBy('name', 'asc')
			->pluck('name', 'id');
	}
}