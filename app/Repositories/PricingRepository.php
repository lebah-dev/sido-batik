<?php

namespace App\Repositories;

use App\Models\Pricing;
use Illuminate\Database\Eloquent\Collection;

class PricingRepository
{
	/**
	 * @return Collection
	 */
	public function getSizeOptions()
	{
		return Pricing::query()
            ->distinct()
            ->select(['width', 'length'])
            ->orderBy('width')
            ->orderBy('length')
            ->get();
	}
}