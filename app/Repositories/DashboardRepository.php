<?php

namespace App\Repositories;

use App\Models\Order;
use App\Models\OrderInvoice;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardRepository
{
	/**
	 * @return int
	 */
	public function getTotalReseller()
	{
		return User::query()->where('role', User::ROLE_RESELLER)->count();
	}

	/**
	 * @return int
	 */
	public function getClaimableOrder()
	{
		return Order::query()->where('reseller_id', Auth::id())
			->where('status', Order::STATUS_FINISHED)
			->whereNull('reseller_claimed_at')
			->count();
	}

	/**
	 * @return int
	 */
	public function getTotalNewOrder()
	{
		return Order::query()
			->when(Auth::user()->role == User::ROLE_RESELLER, function (Builder $query) {
				$query->where('reseller_id', Auth::id());
			})->where('status', Order::STATUS_NEW)->count();
	}

	/**
	 * @return int
	 */
	public function getTotalOngoingOrder()
	{
		return Order::query()
			->when(Auth::user()->role == User::ROLE_RESELLER, function (Builder $query) {
				$query->where('reseller_id', Auth::id());
			})->whereNotIn('status', [Order::STATUS_NEW, Order::STATUS_FINISHED])->count();
	}

	/**
	 * @return int
	 */
	public function getTotalFinishedOrder()
	{
		return Order::query()
			->when(Auth::user()->role == User::ROLE_RESELLER, function (Builder $query) {
				$query->where('reseller_id', Auth::id());
			})->where('status', Order::STATUS_FINISHED)->count();
	}

	/**
	 * @return int
	 */
	public function getTotalPendingDpOrder()
	{
		return Order::query()
			->when(Auth::user()->role == User::ROLE_RESELLER, function (Builder $query) {
				$query->where('reseller_id', Auth::id());
			})->where('status', Order::STATUS_DOWN_PAYMENT)
			->whereHas('invoices', function (Builder $invoices) {
				$invoices->where('type', OrderInvoice::TYPE_DOWN_PAYMENT)
					->where('status', OrderInvoice::STATUS_PENDING);
			})->count();
	}
	
	/**
	 * @return int
	 */
	public function getTotalFinishedDpOrder()
	{
		return Order::query()
			->when(Auth::user()->role == User::ROLE_RESELLER, function (Builder $query) {
				$query->where('reseller_id', Auth::id());
			})->where('status', Order::STATUS_DOWN_PAYMENT)
			->whereHas('invoices', function (Builder $invoices) {
				$invoices->where('type', OrderInvoice::TYPE_DOWN_PAYMENT)
				->where('status', OrderInvoice::STATUS_PAID);
			})->count();
	}
	
	/**
	 * @return int
	 */
	public function getTotalPendingFpOrder()
	{
		return Order::query()
			->when(Auth::user()->role == User::ROLE_RESELLER, function (Builder $query) {
				$query->where('reseller_id', Auth::id());
			})->where('status', Order::STATUS_FINAL_PAYMENT)
			->whereHas('invoices', function (Builder $invoices) {
				$invoices->where('type', OrderInvoice::TYPE_FINAL_PAYMENT)
					->where('status', OrderInvoice::STATUS_PENDING);
			})->count();
	}

	/**
	 * @return int
	 */
	public function getTotalFinishedFpOrder()
	{
		return Order::query()
			->when(Auth::user()->role == User::ROLE_RESELLER, function (Builder $query) {
				$query->where('reseller_id', Auth::id());
			})->where('status', Order::STATUS_FINAL_PAYMENT)
			->whereHas('invoices', function (Builder $invoices) {
				$invoices->where('type', OrderInvoice::TYPE_FINAL_PAYMENT)
					->where('status', OrderInvoice::STATUS_PAID);
			})->count();
	}

	/**
	 * @return Collection
	 */
	public function getNewOrders()
	{
		return Order::query()
			->when(Auth::user()->role == User::ROLE_RESELLER, function (Builder $query) {
				$query->where('reseller_id', Auth::id());
			})
			->where('status', Order::STATUS_NEW)
			->get();
	}

	/**
	 * @return array
	 */
	public function getPlacedOrderGraph()
	{
		$categories = []; $values = [];

		$orders = Order::query()
			->select(DB::raw('count(id) as count'), DB::raw("DATE_FORMAT(created_at, '%Y-%m') as date"))
			->groupBy('date')
			->orderBy('date')
			->get()
			->pluck('count', 'date')
			->toArray();

		for ($i = 0; $i < 6; $i++) {
			$currentDate = today()->subMonths($i);
			$categories[] = $currentDate->isoFormat('MMMM YY');

			$values[] = $orders[$currentDate->format('Y-m')] ?? 0;
		}

		return [
			'categories' => array_reverse($categories),
			'values' => array_reverse($values),
			'step' => (int) collect($values)->median(),
		];
	}
}