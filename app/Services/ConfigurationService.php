<?php

namespace App\Services;

use App\Http\Requests\ConfigurationRequest;
use App\Models\Configuration;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class ConfigurationService
{
	/**
	 * @return ConfigurationService
	 */
	public static function call()
	{
		return new static;
	}

	/**
	 * @param string $key
	 * @return mixed
	 */
	public static function config(string $key)
	{
		return static::call()->get($key);
	}

	/**
	 * @param string $key
	 * @return mixed
	 */
	public function get(string $key)
	{
		$config = Configuration::query()->select('value')->where('key', $key)->first();

		return $config ? $config->value : config('sidobatik.' . $key);
	}

	/**
	 * @param ConfigurationRequest $request
	 * @return void
	 */
	public function update(ConfigurationRequest $request)
	{
		DB::transaction(function () use ($request) {
			foreach($request->validated() as $key => $value) {
				Configuration::query()->updateOrCreate(['key' => $key], ['value' => $value]);
			}
		});
	}

	/**
	 * @return void
	 */
	public function reset()
	{
		Configuration::truncate();
	}
}