<?php

namespace App\Services\Orders;

use App\Exceptions\Orders\StoreOrderException;
use App\Http\Requests\Orders\StoreOrderRequest;
use App\Models\Order;
use App\Notifications\OrderCreated;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;

class StoreOrderService
{
	/**
	 * @var Order
	 */
	private $order;

	/**
	 * @param StoreOrderRequest $data
	 * @return Order
	 */
	public function store(StoreOrderRequest $data)
	{
		try {
			DB::beginTransaction();

			$this->order = new Order([
				'number' => Order::generateNewNumber(),
				'reseller_id' => $data->reseller_id,
				'status' => Order::STATUS_NEW,
	
				'customer' => $data->customer,
				'address' => $data->address,
				'wa_number' => $data->wa_number,
				'email' => $data->email,
				
				'request_description' => $data->request_description,
			]);
			$this->order->save();
			
			if ($data->hasFile('image')) {
				$this->storeImage($data->file('image'), $data->image_description);
			}

			$this->order->notify(new OrderCreated());
			
			DB::commit();
			return $this->order;
		} catch (\Exception $e) {
			DB::rollBack();
			throw new StoreOrderException('Can\'t store order. ' . $e->getMessage());
		}
	}

	/**
	 * @param UploadedFile $imageFile
	 * @param string $description
	 * 
	 * @return void
	 */
	private function storeImage(UploadedFile $imageFile, $description)
	{
		$this->order->images()->create([
            'image_path' => $imageFile->store('order/' . $this->order->number . '/images'),
            'description' => $description,
        ]);
	}
}