<?php

namespace App\Services\Orders;

use App\Exceptions\NoSizeException;
use App\Http\Requests\Orders\FixOrderRequest;
use App\Http\Requests\Orders\SaveShippingData;
use App\Models\Order;
use App\Models\Pricing;
use App\Notifications\DesignFinished;
use App\Notifications\OrderFinished;
use App\Notifications\OrderProcessed;
use App\Notifications\ProductionFinished;
use App\Notifications\ShippingCodeSaved;
use App\Notifications\StatusUpdated;
use App\Services\ConfigurationService;
use Illuminate\Http\Request as HttpRequest;

class ProcessOrderService
{
	/**
	 * Process order staus to next step
	 * 
	 * @param Order $order
	 * @return void
	 */
	public function process(Order $order)
	{
		switch ($order->status) {
			case Order::STATUS_NEW:
				$order->status = Order::STATUS_DESIGN;
				$order->save();

				$order->notify(new OrderProcessed());
				break;
				
			case Order::STATUS_DESIGN:
				$order->status = Order::STATUS_DOWN_PAYMENT;
				$order->save();

				$order->notify(new DesignFinished());
				break;

			case Order::STATUS_DOWN_PAYMENT:
				$order->status = Order::STATUS_PRODUCTION;
				$order->save();

				$order->notify(new StatusUpdated());
				break;

			case Order::STATUS_PRODUCTION:
				$order->status = Order::STATUS_PAINTING;
				$order->save();

				$order->notify(new StatusUpdated());
				break;

			case Order::STATUS_PAINTING:
				$order->status = Order::STATUS_FINAL_PAYMENT;
				$order->save();

				$order->notify(new ProductionFinished());
				break;

			case Order::STATUS_FINAL_PAYMENT:
				$order->status = Order::STATUS_PACKAGING;
				$order->save();
				break;

			case Order::STATUS_PACKAGING:
				$order->status = Order::STATUS_DELIVER;
				$order->save();
				break;

			case Order::STATUS_DELIVER:
				$order->status = Order::STATUS_FINISHED;
				$order->save();

				$order->notify(new OrderFinished());
				break;
			
			default:
				break;
		}
	}

	/**
	 * Fix order by store order quantity
	 * 
	 * @param FixOrderRequest $request
	 * @param Order $order
	 * @return void
	 */
	public function fixOrder(FixOrderRequest $request, Order $order)
	{
		$price = Pricing::query()
			->where('width', (float) explode(' x ', $request->size)[0])
			->where('length', (float) explode(' x ', $request->size)[1])
			->where('min_qty', '<=', (int) $request->quantity)
			->orderBy('min_qty', 'desc')
			->first();

		if (!$price) throw new NoSizeException('No configuration price for size ' . $request->size . ' yet');

		$totalPrice = $request->quantity * $price->unit_price;

		$order->size = $request->size;
		$order->weight = $price->weight;
		$order->unit_price = $price->unit_price;
		$order->quantity = $request->quantity;
		$order->is_design_claimed = true;
		$order->total_price = $totalPrice;
		$order->notes = $request->notes;
		$order->save();
	}

	/**
	 * Save Shipping Data for Delivery
	 * 
	 * @param SaveShippingData $request
	 * @param Order $order
	 * @return void
	 */
	public function saveShippingData(SaveShippingData $request, Order $order)
	{
		$courier = explode('|', $request->courier_package);

		$order->receiver_name = $request->receiver_name;
		$order->receiver_phone = $request->receiver_phone;
		$order->shipping_province = $request->shipping_province;
		$order->shipping_city = $request->shipping_city;
		$order->shipping_address = $request->shipping_address;
		$order->courier_name = $courier[0];
		$order->courier_service = $courier[1];
		$order->courier_estimated_delivery = $courier[2];
		$order->delivery_fee = (int) $courier[3];
		$order->save();
	}

	/**
	 * Save Shipping Code
	 * 
	 * @param string $shippingCode
	 * @param Order $order
	 * @return void
	 */
	public function saveShippingCode(string $shippingCode, Order $order)
	{
		$order->shipping_code = $shippingCode;
		$order->save();

		$order->notify(new ShippingCodeSaved());
	}

	/**
	 * Claim Margin
	 * 
	 * @param Order $order
	 * @return void
	 */
	public function claimMargin(Order $order)
	{
		$order->reseller_claimed_at = now();
		$order->save();
	}

	public function storeDeliveryReceipt(Order $order, HttpRequest $request)
	{
		if ($request->hasFile('delivery_receipt')) {
			$order->delivery_receipt_path = $request->file('delivery_receipt')->store('order/' . $order->number);
			$order->save();
		}
	}
}