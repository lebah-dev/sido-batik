<?php

namespace App\Services\Orders;

use App\Exceptions\Orders\UpdateOrderException;
use App\Http\Requests\Orders\StoreImageRequest;
use App\Http\Requests\Orders\UpdateOrderRequest;
use App\Models\Order;
use App\Models\OrderImage;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class UpdateOrderService
{
	/**
	 * @param UpdateOrderRequest $data
	 * @param Order $order
	 * @return Order
	 * @throws UpdateOrderException
	 */
	public function update(UpdateOrderRequest $data, Order $order)
	{
		try {
			DB::beginTransaction();

			$order->fill([
				'reseller_id' => $data->reseller_id,
	
				'customer' => $data->customer,
				'address' => $data->address,
				'wa_number' => $data->wa_number,
				'email' => $data->email,
				
				'request_description' => $data->request_description,
			]);
			$order->save();
			
			DB::commit();
			return $order;
		} catch (Exception $e) {
			DB::rollBack();
			throw new UpdateOrderException('Can\'t update order. ' . $e->getMessage());
		}
	}

	/**
	 * Store Image
	 * 
	 * @param StoreImageRequest $request
	 * @param Order $order
	 * @return Order
	 * @throws UpdateOrderException
	 */
	public function storeImage(StoreImageRequest $request, Order $order)
	{
		try {
			DB::beginTransaction();

			$order->images()->create([
				'image_path' => $request->file('image')->store('order/' . $order->number . '/images'),
				'description' => $request->image_description,
			]);

			DB::commit();
			return $order;
		} catch (Exception $e) {
			DB::rollBack();
			throw new UpdateOrderException('Can\'t store image to order ' . $order->number . '. ' . $e->getMessage());
		}
	}

	/**
	 * Store Image
	 * 
	 * @param Order $order
	 * @param OrderImage $orderImage
	 * @return Order
	 * @throws UpdateOrderException
	 */
	public function deleteImage(Order $order, OrderImage $orderImage)
	{
		try {
			DB::beginTransaction();

			Storage::delete($orderImage->image_path);
			$orderImage->delete();

			DB::commit();
			return $order;
		} catch (Exception $e) {
			DB::rollBack();
			throw new UpdateOrderException('Can\'t delete image from order ' . $order->number . '. ' . $e->getMessage());
		}
	}
}