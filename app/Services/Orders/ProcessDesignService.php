<?php

namespace App\Services\Orders;

use App\Exceptions\Orders\ProcessDesignException;
use App\Http\Requests\Orders\ReviseDesignRequest;
use App\Http\Requests\Orders\StoreReviseAttachmentRequest;
use App\Models\Order;
use App\Models\OrderDesign;
use App\Models\OrderDesignRevision;
use App\Models\OrderDesignRevisionAttachment;
use Illuminate\Support\Facades\DB;

class ProcessDesignService
{
	/**
	 * store order service and create order sign revision
	 * 
	 * @param ReviseDesignRequest $request
	 * @param Order $order
	 * @param OrderDesign $design
	 * @return void
	 */
	public function revise(ReviseDesignRequest $request, Order $order, OrderDesign $design)
	{
		try {
			DB::beginTransaction();

			$revision = $design->revision()->create([
				'text' => $request->text
			]);
			
			if ($request->hasFile('attachment')) {
				$revision->attachments()->create([
					'file_path' => $request->file('attachment')->store('order/' . $order->number . '/designs/revisions'),
					'description' => $request->text
				]);
			}

			$design->status = OrderDesign::STATUS_REJECT;
			$design->save();

			DB::commit();
		} catch (\Exception $e) {
			DB::rollBack();
			throw new ProcessDesignException('Can\'t process revise design. ' . $e->getMessage());
		}
	}

	/**
	 * Store order design revision attachment
	 * 
	 * @param StoreReviseAttachmentRequest $request
	 * @param Order $order
	 * @param OrderDesign $design
	 * @param OrderDesignRevision $revision
	 * @return void
	 */
	public function storeRevision(StoreReviseAttachmentRequest $request, Order $order, OrderDesignRevision $revision)
	{
		OrderDesignRevisionAttachment::query()->create([
			'file_path' => $request->file('attachment')->store('order/' . $order->number . '/designs/revisions'),
			'description' => $request->description,
			'design_revision_id' => $revision->id
		]);
	}

	/**
	 * Approve design
	 * 
	 * @param OrderDesign $design
	 * @return void
	 */
	public function approve(OrderDesign $design)
	{
		$design->status = OrderDesign::STATUS_APPROVE;
		$design->save();
	}
}