<?php

namespace App\Services\Orders;

use App\Http\Requests\Orders\MarkAsPaidRequest;
use App\Models\Order;
use App\Models\OrderInvoice;
use App\Notifications\NewInvoiceCreated;
use App\Services\ConfigurationService;
use Carbon\Carbon;

class OrderInvoiceService
{
	/**
	 * @param Order $order
	 * @param string $type
	 * @return void
	 */
	public function createInvoice(Order $order, string $type)
	{
		$invoice = OrderInvoice::query()->updateOrCreate([
			'order_id' => $order->id,
			'type' => $type
		], [
			'number' => OrderInvoice::generateNumber(),
			'amount' => $type == OrderInvoice::TYPE_FINAL_PAYMENT
				? ($order->total_price * 0.5) + $order->delivery_fee
				: $order->total_price * 0.5,
			'payment_code' => rand(0, 499),
			'due_date' => Carbon::now()->addDays(ConfigurationService::config('invoice_due_days')),
			'status' => OrderInvoice::STATUS_PENDING,
		]);

		$order->notify(new NewInvoiceCreated($invoice));
	}

	/**
	 * @param OrderInvoice $invoice
	 * @return void
	 */
	public function paid(OrderInvoice $invoice, MarkAsPaidRequest $request)
	{
		$file = $request->file('payment_receipt');

		$invoice->payment_receipt_path = $file
			->storeAs('order/' . $invoice->order->number, $invoice->number . '.' . $file->getClientOriginalExtension());
		$invoice->status = OrderInvoice::STATUS_PAID;
		$invoice->paid_at = now();
		$invoice->save();
	}

	/**
	 * @param OrderInvoice $invoice
	 * @return voide
	 */
	public function reset(OrderInvoice $invoice)
	{
		$invoice->paid_at = null;
		$invoice->status = OrderInvoice::STATUS_PENDING;
		$invoice->payment_receipt_path = null;
		$invoice->save();

		$invoice->order->notify(new NewInvoiceCreated($invoice));
	}
}