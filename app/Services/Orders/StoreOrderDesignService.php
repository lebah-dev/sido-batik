<?php

namespace App\Services\Orders;

use App\Http\Requests\Orders\StoreOrderDesignRequest;
use App\Models\Order;
use App\Models\OrderDesign;
use App\Models\OrderDesignRevision;
use App\Notifications\NewDesignUploaded;

class StoreOrderDesignService
{
	/**
	 * Store order design
	 * 
	 * @param StoreOrderDesignRequest $request
	 * @param Order $order
	 * 
	 * @return $order
	 */
	public function store(StoreOrderDesignRequest $request, Order $order)
	{
		$revision = $this->getLastRejectedDesignRevision($order);

		$order->designs()->create([
			'version' => $version = OrderDesign::getNextDesignVerision($order->id),
			'file_path' => $request->file('file')->storeAs('order/' . $order->number . '/designs', 'design-v' . $version . '.' . $request->file('file')->getClientOriginalExtension()),
			'status' => OrderDesign::STATUS_REVIEW,
			'revision_id' => $revision ? $revision->id : null,
		]);

		$order->notify(new NewDesignUploaded());
		return $order;
	}

	/**
	 * Get last revision from rejected order design
	 * 
	 * @param Order $order
	 * @return OrderDesignRevision|null
	 */
	private function getLastRejectedDesignRevision(Order $order)
	{
		$design = $order->designs()->where('status', OrderDesign::STATUS_REJECT)->latest()->first();

		return $design ? $design->revision : null;
	}
}