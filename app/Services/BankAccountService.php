<?php

namespace App\Services;

use App\Http\Requests\UserManagement\UpdateBankAccountRequest;
use App\Models\BankAccount;
use App\Models\User;

class BankAccountService
{
	/**
	 * @param User $user
	 * @param UpdateBankAccountRequest $request
	 * 
	 * @return BankAccount
	 */
	public function update(User $user, UpdateBankAccountRequest $request)
	{
		return BankAccount::query()->updateOrCreate([
			'id' => $user->id,
		], [
			'name' => $request->name,
			'bank' => $request->bank,
			'number' => $request->number,
		]);
	}
}