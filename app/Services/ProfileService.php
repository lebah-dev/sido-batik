<?php

namespace App\Services;

use App\Http\Requests\UpdateProfileRequest;
use App\Models\ResellerProfile;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class ProfileService
{
	/**
	 * @param UpdateProfileRequest $request
	 * @param User $user
	 * 
	 * @return ResellerProfile
	 */
	public function update(UpdateProfileRequest $request, User $user)
	{
		if (!$user->profile) {
			$request->validate([
				'portrait_photo' => 'required|image|max:2000',
            	'identity_scan' => 'required|image|max:2000',
			]);
		}

		$portraitPhotoPath = optional($user->profile)->portrait_photo_path;
		$identityScanPath = optional($user->profile)->identity_scan_path;

		if ($request->hasFile('portrait_photo')) {
			Storage::delete(optional($user->profile)->portrait_photo_path);
			$portraitPhotoPath = $request->file('portrait_photo')->store('users/' . $user->email);
		}
		
		if ($request->hasFile('identity_scan')) {
			Storage::delete(optional($user->profile)->identity_scan_path);
			$identityScanPath = $request->file('identity_scan')->store('users/' . $user->email);
		}

		$code = optional($user->profile)->code_reference ?? sprintf("%06d", mt_rand(1, 999999));
		return $user->profile()->updateOrCreate([
			'id' => $user->id
		], [
			'full_name' => $request->full_name,
			'identity_number' => $request->identity_number,
			'wa_number' => $request->wa_number,
			'facebook_account' => $request->facebook_account,
			'portrait_photo_path' => $portraitPhotoPath,
			'identity_scan_path' => $identityScanPath,
			'verified_at' => null,
			'code_reference' => $code,
		]);
	}

	/**
	 * @param User $user
	 * @return void
	 */
	public function verify(User $user)
	{
		if ($user->profile()->exists()) {
			$user->profile()->update(['verified_at' => Carbon::now()]);
		}
	}
}