<?php

namespace App\Services;

use App\Http\Requests\UserManagement\UpdateUserAccountRequest;
use App\Models\User;

class UserManagementService
{
	/**
	 * @param UpdateUserAccountRequest $request
	 * @param User $user
	 * 
	 * @return void
	 */
	public function update(UpdateUserAccountRequest $request, User $user)
	{
		$user->fill($request->validated());

		if ($request->has('password') && $request->get('password')) {
			$request->validate(['password' => 'confirmed']);

			$user->password = bcrypt($request->get('password'));
		}

		$user->save();
	}
}