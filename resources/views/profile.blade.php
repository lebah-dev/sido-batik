<x-app-layout>
	<x-slot name="header">Profile</x-slot>
	
	<ul class="nav nav-pills mb-4">
		<li class="nav-item">
			<a class="nav-link active" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><i class="fas fa-user"></i> Profile Detail</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><i class="fas fa-credit-card"></i> Bank Account</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false"><i class="fas fa-cog"></i> Account Setting</a>
		</li>
	</ul>

	<div class="tab-content" id="myTabContent">
		<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
			<x-users.profile :user="$user"></x-users.profile>
		</div>
		<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
			<x-users.bank-account :user="$user"></x-users.bank-account>
		</div>
		<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
			<x-users.account :user="$user"></x-users.account>
		</div>
	</div>
</x-app-layout>