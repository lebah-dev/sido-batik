<x-app-layout>
	<x-slot name="cssLibs">
		<link rel="stylesheet" href="{{ asset('assets/modules/summernote/summernote-bs4.css') }}">
	</x-slot>

	<x-slot name="header">Formlir Pesanan</x-slot>

	<div class="row">
		<div class="col-12">
			<form action="{{ route('order-managements.update', $order) }}" method="post">
				@csrf @method('put')
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Perbaharui Pesanan</h4>
					</div>
					<div class="card-body">
						<div class="form-group row">
							<div class="col-md-2 col-form-label">Reseller</div>
							<div class="col-md-6">
								@if(auth()->user()->isAdmin())
								<select name="reseller_id" class="form-control @error('reseller_id') is-invalid @enderror">
									<option value="">-- Reseller --</option>
									@foreach($resellers as $id => $name)
									<option value="{{ $id }}" {{ $order->reseller_id == $id ? 'selected' : '' }}>{{ $name }}</option>
									@endforeach
								</select>
								@else
								<div class="py-2">{{ auth()->user()->name }}</div>
								<input type="hidden" name="reseller_id" value="{{ auth()->id() }}" class="form-control @error('reseller_id') is-invalid @enderror" readonly>
								@endif
								@error('reseller_id') <div class="invalid-feedback">{{ $message }}</div> @enderror
							</div>
						</div>
						<hr>
						<div class="form-group row">
							<label class="col-md-2 col-form-label">Nama</label>
							<div class="col-md-4">
								<input type="text" name="customer" value="{{ old('customer') ?? $order->customer }}" class="form-control @error('customer') is-invalid @enderror">
								@error('customer') <div class="invalid-feedback">{{ $message }}</div> @enderror
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-2 col-form-label">Alamat</label>
							<div class="col-md-8">
								<textarea name="address" class="form-control @error('address') is-invalid @enderror">{{ old('address') ?? $order->address }}</textarea>
								@error('address') <div class="invalid-feedback">{{ $message }}</div> @enderror
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-2 col-form-label">WhatsApp</label>
							<div class="col-md-3">
								<input type="text" name="wa_number" value="{{ old('wa_number') ?? $order->wa_number }}" class="form-control @error('wa_number') is-invalid @enderror">
								@error('wa_number') <div class="invalid-feedback">{{ $message }}</div> @enderror
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-2 col-form-label">Email</label>
							<div class="col-md-3">
								<input type="email" name="email" value="{{ old('email') ?? $order->email }}" class="form-control @error('email') is-invalid @enderror">
								@error('email') <div class="invalid-feedback">{{ $message }}</div> @enderror
							</div>
						</div>
						<hr>
						<div class="form-group row">
							<label class="col-md-2 col-form-label">Deskripsi Permintaan</label>
							<div class="col-md-8">
								<textarea name="request_description" class="summernote-simple form-control @error('request_description') is-invalid @enderror">{{ old('request_description') ?? $order->request_description }}</textarea>
								@error('request_description') <div class="invalid-feedback">{{ $message }}</div> @enderror
							</div>
						</div>
					</div>
					<div class="card-footer text-right">
						<a href="{{ route('user-managements.index') }}" class="btn btn-light"><i class="fas fa-chevron-left"></i> Batal</a>
						<button class="btn btn-success"><i class="fas fa-plus"></i> Simpan</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<x-slot name="jsLibs">
		<script src="{{ asset('assets/modules/summernote/summernote-bs4.js') }}"></script>
	</x-slot>
</x-app-layout>