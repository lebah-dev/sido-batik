<x-app-layout>
	<x-slot name="header">Manajement Pesanan</x-slot>

	<div class="row">
		<div class="col-lg-12 mb-4">
			<ul class="nav nav-pills justify-content-between">
				<li class="nav-item">
					<a class="nav-link {{ !request()->has('status') ? 'active' : '' }}" href="{{ route('order-managements.index', ['status' => null]) }}">Semua</a>
				</li>
				@foreach($statuses as $status)
				<li class="nav-item">
					<a class="nav-link {{ request('status') == $status ? 'active' : '' }}" href="{{ route('order-managements.index', ['status' => $status]) }}">{{ strtoupper(__($status)) }}</a>
				</li>
				@endforeach
			</ul>
		</div>
		<div class="col-lg-12 mb-4">
			<div class="card">
				<div class="card-body">
					{{ $dataTable->table() }}
				</div>
			</div>
		</div>
	</div>

	<x-datatable-assets :data-table="$dataTable"></x-datatable-assets>
</x-app-layout>