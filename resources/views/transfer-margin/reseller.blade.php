<x-app-layout>
	<x-slot name="header">Margin Claims</x-slot>

	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Finished Orders</h4>
				</div>
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>Created At</th>
								<th>Number</th>
								<th>Quantity</th>
								<th>Claimable</th>
								<th>Claimed At</th>
							</tr>
						</thead>
						<tbody>
							@foreach($orders as $order)
							<tr>
								<td>{{ $order->created_at->format('d M Y H:i') }}</td>
								<td><a href="{{ route('order-managements.show', $order) }}">{{ $order->number }}</a></td>
								<td>{{ $order->quantity }}</td>
								<td>Rp. {{ number_format($order->reseller_claimable) }}.-</td>
								<td>{{ $order->reseller_claimed_at ? $order->reseller_claimed_at->format('d M Y H:i') : 'On Process' }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</x-app-layout>