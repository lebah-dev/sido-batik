<x-app-layout>
	<x-slot name="header">Transfer Margin</x-slot>

	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Resellers</h4>
				</div>
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>Name</th>
								<th>Orders</th>
								<th>Total</th>
							</tr>
						</thead>
						<tbody>
							@foreach($resellers as $reseller)
							<tr>
								<td>
									<a href="{{ route('user-managements.show', $reseller) }}">
										{{ $reseller->name }}
									</a>
								</td>
								<td>
									<a href="{{ route('order-managements.index', ['reseller' => $reseller->id, 'status' => 'finished', 'claimed' => false]) }}">
										{{ $reseller->orders()->finished()->unclaimed()->count() }} Finished Order
									</a>
								</td>
								<td>Rp. {{ number_format($reseller->getUnclaimedMargin()) }}.-</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</x-app-layout>