<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
	<title>{{ $header ?? 'Home' }} &mdash; {{ config('app.name') }}</title>

	<!-- General CSS Files -->
	<link rel="stylesheet" href="{{ asset('assets/modules/fontawesome/css/all.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/modules/bootstrap/css/bootstrap.min.css') }}">

	<!-- CSS Libraries -->
	<link rel="stylesheet" href="{{ asset('assets/modules/bootstrap-social/bootstrap-social.css') }}">
	{{ $cssLibs ?? null }}

	<!-- Template CSS -->
	<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">

	<!-- Page Specific Style -->
	@stack('styles')
</head>

<body>
	<div id="app">
		<div class="main-wrapper main-wrapper-1">
			<x-navbar></x-navbar>
			<x-sidebar></x-sidebar>

			<!-- Main Content -->
			<div class="main-content">
				<section class="section">
					<div class="section-header">
						<h1>{{ $header }}</h1>
					</div>

					<div class="section-body">
						<x-alert></x-alert>

						{{ $slot }}
					</div>
				</section>
			</div>

			<footer class="main-footer">
				<div class="footer-left">
					Copyright &copy; 2021 <div class="bullet"></div> {{ config('app.name') }}</a>
				</div>
				<div class="footer-right">

				</div>
			</footer>
		</div>

		@stack('modals')
	</div>

	<!-- General JS Scripts -->
	<script src="{{ asset('assets/modules/jquery.min.js') }}"></script>
	<script src="{{ asset('assets/modules/popper.js') }}"></script>
	<script src="{{ asset('assets/modules/tooltip.js') }}"></script>
	<script src="{{ asset('assets/modules/bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('assets/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
	<script src="{{ asset('assets/modules/moment.min.js') }}"></script>
	<script src="{{ asset('assets/js/stisla.js') }}"></script>

	<!-- JS Libraies -->
	{{ $jsLibs ?? null }}

	<!-- Page Specific JS File -->
	@stack('scripts')

	<!-- Template JS File -->
	<script src="{{ asset('assets/js/scripts.js') }}"></script>
	<script src="{{ asset('assets/js/custom.js') }}"></script>
</body>

</html>