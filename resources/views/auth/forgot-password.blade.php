<x-auth-layout>
	<x-slot name="title">Forgot Password</x-slot>
	<div class="card card-primary">
		<div class="card-header"><h4>Forgot Password</h4></div>

		<div class="card-body">
			<p class="text-muted">We will send a link to reset your password</p>
			<form method="POST" action="{{ route('password.email') }}">
				@csrf

				<div class="form-group">
					<label for="email">Email</label>
					<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" tabindex="1" autofocus>
					@error('email') <div class="invalid-feedback"></div> @enderror
					@if(session('status')) <div class="text-success">{{ __(session('status')) }}</div> @endif
				</div>

				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
						Forgot Password
					</button>
				</div>
			</form>
		</div>
	</div>
</x-auth-layout>