<x-auth-layout>
	<x-slot name="title">Join Reseller</x-slot>

	<div class="card card-primary">
		<div class="card-header">
			<h4>Register Reseller</h4>
		</div>

		<div class="card-body">
			<form method="POST" action="{{ route('register') }}">
				@csrf

				<div class="form-group">
					<label for="name">Full Name</label>
					<input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" tabindex="1" autofocus>
					@error('name') <div class="invalid-feedback">{{ __($message) }}</div> @enderror
				</div>

				<div class="form-group">
					<label for="email">Email</label>
					<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" tabindex="1" autofocus>
					@error('email') <div class="invalid-feedback">{{ __($message) }}</div> @enderror
				</div>

				<div class="form-group">
					<label for="password" class="control-label">Password</label>
					<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" tabindex="2">
					@error('password') <div class="invalid-feedback">{{ __($message) }}</div> @enderror
				</div>

				<div class="form-group">
					<label for="password_confirmation" class="control-label">Password Confirmation</label>
					<input id="password_confirmation" type="password" class="form-control @error('password') is-invalid @enderror" name="password_confirmation" tabindex="2">
					@error('password') <div class="invalid-feedback">{{ __($message) }}</div> @enderror
				</div>

				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
						Register
					</button>
				</div>
			</form>

		</div>
	</div>
</x-auth-layout>