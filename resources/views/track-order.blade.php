<x-layouts.customer>
	<x-slot name="cssLibs">
		<link rel="stylesheet" href="{{ asset('assets/modules/select2/dist/css/select2.min.css') }}">
	</x-slot>

	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body pb-0">
					<div class="row">
						<div class="col-lg-12">
							<form action="{{ route('track-order') }}" method="get">
								<div class="form-group">
									<div class="input-group mb-3">
										<input type="text" name="reseller" class="form-control @if (request('reseller') && $order == null) is-invalid @endif" value="{{ request('reseller') }}" placeholder="Reseller Code">
										<input type="text" name="order" class="form-control @if (request('order') && $order == null) is-invalid @endif" value="{{ request('order') }}" placeholder="Order Number">
										<div class="input-group-append">
											<button class="btn btn-primary btn-icon icon-left"><i class="fas fa-search"></i><span class="d-none d-md-inline">Search</span></button>
										</div>
									</div>
									@if (request('order') && $order == null) <div class="invalid-feedback">Order not found, please check your order number. If it still not found, please contact the seller.</div> @endif
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		@if ($order)
		<div class="col-lg-9">
			<div class="row">
				@if($order->status == 'finished')
				<div class="col-lg-12">
					<x-orders.feedback></x-orders.feedback>
				</div>
				@endif
				<div class="col-lg-12">
					<x-orders.detail :order="$order"></x-orders.detail>
				</div>
				@if(in_array($order->status, ['deliver', 'finished']))
				<div class="col-lg-12">
					<x-orders.delivery-receipt :order="$order"></x-orders.delivery-receipt>
				</div>
				@endif
				@if($order->passPayment())
				<div class="col-lg-12">
					<x-orders.order-shipping-code :order="$order"></x-orders.order-shipping-code>
				</div>
				@endif
				@if($order->passProduction())
				<div class="col-lg-12">
					<x-orders.shipping-data :order="$order"></x-orders.shipping-data>
				</div>
				@endif
				@if($order->passDesign())
				<div class="col-lg-12">
					<x-orders.fixing-order :order="$order"></x-orders.fixing-order>
				</div>
				@endif
				<div class="col-lg-12">
					<x-orders.order-request :order="$order"></x-orders.order-request>
				</div>
				@if ($order->status != 'new')
				<div class="col-lg-12">
					<x-orders.order-design :order="$order"></x-orders.order-design>
				</div>
				@endif
			</div>
		</div>
		<div class="col-lg-3">
			<div class="row">
				@if (auth()->check())
				<div class="col-lg-12">
					<x-orders.order-actions :order="$order"></x-orders.order-actions>
				</div>
				@endif
				@if ($order->passDesign() && $order->quantity > 0)
				<div class="col-lg-12">
					<x-orders.order-invoices :order="$order"></x-orders.order-invoices>
				</div>
				@endif
				<hr>
				<div class="col-lg-12">
					<x-orders.order-customer :order="$order"></x-orders.order-customer>
				</div>
				<div class="col-lg-12">
					<x-orders.order-reseller :order="$order"></x-orders.order-reseller>
				</div>
			</div>
		</div>
		@endif
	</div>

	<x-slot name="jsLibs">
		<script src="{{ asset('assets/modules/select2/dist/js/select2.full.min.js') }}"></script>
	</x-slot>
</x-layouts.customer>