<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
	<title>Print Invoice &mdash; {{ config('app.name') }}</title>

	<!-- General CSS Files -->
	<link rel="stylesheet" href="{{ asset('assets/modules/fontawesome/css/all.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/modules/bootstrap/css/bootstrap.min.css') }}">

	<!-- CSS Libraries -->
	<link rel="stylesheet" href="{{ asset('assets/modules/bootstrap-social/bootstrap-social.css') }}">

	<!-- Template CSS -->
	<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">

	<!-- Page Specific Style -->
	<style>
		
		body {
			font-family: 'Times New Roman', Times, serif;
			font-size: 12pt;
			line-height: 1.3;
			color: black;
			background: white;
		}

		@page {
			size: A4 portrait;
		}

	</style>
</head>

<body>
	<div id="app">
		<div id="invoice-print" class="invoice-print">
			<div class="row">
				<div class="col-lg-12">
					<div class="invoice-title">
						<h2>Faktur {{ ucwords(str_replace('-', ' ', __($invoice->type))) }}</h2>
						<div class="invoice-number">{{ $invoice->number }}</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-6">
							<address>
								<strong>Ditagih Ke:</strong><br>
								{{ $invoice->order->customer }}<br>
								{{ $invoice->order->address }}<br>
								{{ $invoice->order->wa_number }}
							</address>
						</div>
						<div class="col-md-6 text-md-right">
							@if($invoice->type == 'final-payment')
							<address>
								<strong>Dikirim Ke:</strong><br>
								{{ $invoice->order->receiver_name }}<br>
								{{ $invoice->order->shipping_address }}<br>
								{{ $invoice->order->receiver_phone }}
							</address>
							@endif
							<address>
								<strong>Tanggal Pesanan:</strong><br>
								{{ $invoice->order->created_at->format('d F Y') }}<br><br>
							</address>
							@if($invoice->paid_at != null)
							<address>
								<strong>Dibayar Pada:</strong><br>
								{{ $invoice->paid_at->format('d F Y') }}<br><br>
							</address>
							@endif
						</div>
					</div>
				</div>
			</div>
			
			<div class="row mt-4">
				<div class="col-md-12">
					<div class="section-title">Deskripsi Permintaan</div>
					<p class="section-lead"></p>
					<div>
						{!! $invoice->order->request_description !!}
					</div>
					<div class="">
						<table class="table table-borderless table-md"><tbody>
							<tr>
								<th>Item</th>
								<th>Harga</th>
								<th style="width: 1px;">Jumlah</th>
								<th style="width: 5cm;" class="text-center">Total</th>
							</tr>
							<tr>
								<td>Custom Batik</td>
								<td>Rp. {{ $invoice->order->unit_price }}.-</td>
								<td>{{ $invoice->order->quantity }}</td>
								<td class="text-right text-nowrap">Rp. {{ number_format($invoice->order->total_price) }}.-</td>
							</tr>
							<tr><td colspan="4"></td></tr>
							<tr><td colspan="4"></td></tr>
							<tr>
								<td></td>
								<td></td>
								<td class="font-weight-bold text-nowrap">Sub Total</td>
								<td class="text-right text-nowrap">Rp. {{ number_format($invoice->order->total_price) }}.-</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td class="font-weight-bold text-nowrap">{{ ucwords(str_replace('-', ' ', $invoice->type)) }} (50%)</td>
								<td class="text-right text-nowrap">Rp. {{ number_format($invoice->order->total_price * 0.5) }}.-</td>
							</tr>
							@if($invoice->type == 'final-payment')
							<tr>
								<td></td>
								<td></td>
								<td class="font-weight-bold text-nowrap">Biaya Kirim</td>
								<td class="text-right text-nowrap">Rp. {{ number_format($invoice->order->delivery_fee) }}.-</td>
							</tr>
							@endif
							<hr class="mt-2 mb-2">
							<tr>
								<td></td>
								<td></td>
								<td class="font-weight-bold text-nowrap">Total</td>
								<td class="text-right text-nowrap">Rp. {{ number_format($invoice->amount) }}.-</td>
							</tr>
						</tbody></table>
					</div>

					<hr class="my-5">

					<div class="row mt-4">
						<div class="col-lg-8">
							<div class="section-title">Metode Pembayaran</div>
							<p class="section-lead">Silahkan melakukan pembayaran sesuai dengan total yang tertera ke salah satu bank di bawah ini.</p>
							<div class="d-flex">
								@foreach ($banks as $bank)
								<address class="mr-5">
									<div class="h6">{{ $bank->bank }}</div>
									<div class="h5 font-weight-bold">{{ $bank->number }}</div>
									<div>{{ $bank->name }}</div>
								</address>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- General JS Scripts -->
	<script src="{{ asset('assets/modules/jquery.min.js') }}"></script>
	<script src="{{ asset('assets/modules/popper.js') }}"></script>
	<script src="{{ asset('assets/modules/tooltip.js') }}"></script>
	<script src="{{ asset('assets/modules/bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('assets/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
	<script src="{{ asset('assets/modules/moment.min.js') }}"></script>
	<script src="{{ asset('assets/js/stisla.js') }}"></script>

	<!-- JS Libraies -->

	<!-- Page Specific JS File -->
	<script>
		$(document).ready(() => {
			window.print()
		})
	</script>

	<!-- Template JS File -->
	<script src="{{ asset('assets/js/scripts.js') }}"></script>
	<script src="{{ asset('assets/js/custom.js') }}"></script>
</body>

</html>