<x-app-layout>
	<x-slot name="header">Faktur Pesanan</x-slot>
	
	<div class="invoice">
		<div id="invoice-print" class="invoice-print">
			<div class="row">
				<div class="col-lg-12">
					<div class="invoice-title">
						<h2>Faktur {{ ucwords(str_replace('-', ' ', __($invoice->type))) }}</h2>
						<div class="invoice-number">{{ $invoice->number }}</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-6">
							<address>
								<strong>Ditagih ke:</strong><br>
								{{ $invoice->order->customer }}<br>
								{{ $invoice->order->address }}<br>
								{{ $invoice->order->wa_number }}
							</address>
						</div>
						<div class="col-md-6 text-md-right">
							@if($invoice->type == 'final-payment')
							<address>
								<strong>Dikirim ke:</strong><br>
								{{ $invoice->order->receiver_name }}<br>
								{{ $invoice->order->shipping_address }}<br>
								{{ $invoice->order->receiver_phone }}
							</address>
							@endif
							<address>
								<strong>Tanggal Pesanan:</strong><br>
								{{ $invoice->order->created_at->format('d F Y') }}<br><br>
							</address>
							@if($invoice->paid_at != null)
							<address>
								<strong>Dibayar pada:</strong><br>
								{{ $invoice->paid_at->format('d F Y') }}<br><br>
							</address>
							@endif
						</div>
					</div>
				</div>
			</div>
			
			<div class="row mt-4">
				<div class="col-md-12">
					<div class="section-title">Deskripsi Permintaan</div>
					<p class="section-lead"></p>
					<div>
						{!! $invoice->order->request_description !!}
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-hover table-md">
							<tbody>
								<tr>
									<th>Item</th>
									<th>Harga</th>
									<th style="width: 1px;">Jumlah</th>
									<th style="width: 5cm;" class="text-center">Total</th>
								</tr>
								<tr>
									<td>Custom Batik</td>
									<td>Rp. {{ number_format($invoice->order->unit_price) }}.-</td>
									<td>{{ $invoice->order->quantity }}</td>
									<td class="text-right text-nowrap">Rp. {{ number_format($invoice->order->total_price) }}.-</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="row mt-4">
						<div class="col-lg-8">
							<div class="section-title">Metode Pembayaran</div>
							<p class="section-lead">Silahkan melakukan pembayaran sesuai dengan total yang tertera ke salah satu bank di bawah ini.</p>
							<div class="pl-5 d-flex justify-content-around">
								@foreach ($banks as $bank)
								<address>
									<div class="h6">{{ $bank->bank }}</div>
									<div class="h5 font-weight-bold">{{ $bank->number }}</div>
									<div>{{ $bank->name }}</div>
								</address>
								@endforeach
							</div>
						</div>
						<div class="col-lg-4 text-right">
							<div class="invoice-detail-item">
								<div class="invoice-detail-name">Sub Total</div>
								<div class="invoice-detail-value">Rp. {{ number_format($invoice->order->total_price) }}.-</div>
							</div>
							<div class="invoice-detail-item">
								<div class="invoice-detail-name">{{ ucwords(str_replace('-', ' ', $invoice->type)) }} (50%)</div>
								<div class="invoice-detail-value">Rp. {{ number_format($invoice->order->total_price * 0.5) }}.-</div>
							</div>
							@if($invoice->type == 'final-payment')
							<div class="invoice-detail-item">
								<div class="invoice-detail-name">Biaya Kirim</div>
								<div class="invoice-detail-value">Rp. {{ number_format($invoice->order->delivery_fee) }}.-</div>
							</div>
							@endif
							<div class="invoice-detail-item">
								<div class="invoice-detail-name">Kode Pembayaran</div>
								<div class="invoice-detail-value">Rp. {{ number_format($invoice->payment_code) }}.-</div>
							</div>
							<hr class="mt-2 mb-2">
							<div class="invoice-detail-item">
								<div class="invoice-detail-name">Total</div>
								<div class="invoice-detail-value invoice-detail-value-lg">Rp. {{ number_format($invoice->amount + $invoice->payment_code) }}.-</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="text-md-right">
			<div class="float-lg-left mb-lg-0 mb-3">
				@if($invoice->paid_at == null)
					<button type="button" class="btn btn-success btn-icon icon-left" data-toggle="modal" data-target="#mark-as-paid"><i class="fas fa-credit-card"></i> Tandai Lunas</button>
				@else
					<button type="button" class="btn btn-danger btn-icon icon-left" data-confirm="{{ __('modal.reset-invoice') }}" data-confirm-yes="$('#reset-invoice').submit();"><i class="fa fa-clock"></i> Reset</button>
				@endif
				<a href="{{ route('order-managements.show', $invoice->order) }}" class="btn btn-secondary btn-icon icon-left"><i class="fas fa-chevron-left"></i> Kembali</a>
				@if($invoice->payment_receipt)<a href="{{ $invoice->payment_receipt }}" target="_blank" class="ml-2">Download Bukti Pembayaran</a>@endif
			</div>
			<a href="{{ route('order-invoice.print', $invoice) }}" target="_blank" class="btn btn-warning btn-icon icon-left"><i class="fas fa-print"></i> Print</a>
		</div>
	</div>

	@if($invoice->paid_at != null)
	<form id="reset-invoice" action="{{ route('order-invoice.reset', $invoice) }}" class="hidden" method="post">@csrf @method('put')</form>
	@endif

	<x-modal id="mark-as-paid" title="Upload Bukti Pembayaran">
		<form action="{{ route('order-invoice.paid', $invoice) }}" method="post" enctype="multipart/form-data">@csrf
			<div class="form-group">
				<label>Bukti Pembayaran</label>
				<input type="file" name="payment_receipt" class="form-control-file">
			</div>
			<div class="form-group text-right">
				<button class="btn btn-success btn-icon icon-left"><i class="fas fa-credit-card"></i> Tandai Lunas</button>
			</div>
		</form>
	</x-modal>
</x-app-layout>