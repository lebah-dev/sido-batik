<div class="row">
	<div class="col-lg-8 col-md-12 col-12 col-sm-12">
		<div class="card">
			<div class="card-header">
				<h4>Pesanan Baru</h4>
			</div>
			<div class="card-body"><div class="chartjs-size-monitor" style="position: absolute; inset: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
				<canvas id="myChart" height="404" width="666" class="chartjs-render-monitor" style="display: block; width: 666px; height: 404px;"></canvas>
				{{-- <div class="statistic-details mt-sm-4">
					<div class="statistic-details-item">
						<span class="text-muted"><span class="text-primary"><i class="fas fa-caret-up"></i></span> 7%</span>
						<div class="detail-value">Rp. {{ number_format(75) }}Jt</div>
						<div class="detail-name">This Month's Sales</div>
					</div>
					<div class="statistic-details-item">
						<span class="text-muted"><span class="text-danger"><i class="fas fa-caret-down"></i></span> 23%</span>
						<div class="detail-value">Rp. {{ number_format(354) }}Jt</div>
						<div class="detail-name">This Quarter's Sales</div>
					</div>
					<div class="statistic-details-item">
						<span class="text-muted"><span class="text-primary"><i class="fas fa-caret-up"></i></span>9%</span>
						<div class="detail-value">Rp. {{ number_format(648) }}Jt</div>
						<div class="detail-name">This Half Year's Sales</div>
					</div>
					<div class="statistic-details-item">
						<span class="text-muted"><span class="text-primary"><i class="fas fa-caret-up"></i></span> 19%</span>
						<div class="detail-value">Rp. {{ number_format(1.45, 2) }}M</div>
						<div class="detail-name">This Year's Sales</div>
					</div>
				</div> --}}
			</div>
		</div>
	</div>

	<div class="col-lg-4 col-md-12 col-12 col-sm-12">
		<div class="card">
			<div class="card-header">
				<h4>Pesanan Baru</h4>
			</div>
			<div class="card-body">             
				<ul class="list-unstyled list-unstyled-border">
					@forelse ($newOrders as $order)
					<li class="media">
						<img class="mr-3 rounded-circle" width="50" src="assets/img/avatar/avatar-1.png" alt="avatar">
						<div class="media-body">
							<div class="float-right text-primary">{{ $order->created_at->diffForHumans() }}</div>
							<div class="media-title">{{ $order->customer }}</div>
							<span class="text-small text-muted">{{ Str::limit(strip_tags($order->request_description), 115, '...') }}</span>
						</div>
					</li>
					@empty
					@endforelse
				</ul>
				<div class="text-center pt-1 pb-1">
					<a href="{{ route('order-managements.index', ['status' => 'new']) }}" class="btn btn-primary btn-lg btn-round">
						Lihat Semua
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

@push('scripts')
<script src="{{ asset('assets/modules/chart.min.js') }}"></script>
<script>
	var statistics_chart = document.getElementById("myChart").getContext('2d');

	var myChart = new Chart(statistics_chart, {
		type: 'line',
		data: {
			labels: @json($placedOrderGraph['categories']),
			datasets: [{
				label: 'Statistics',
				data: @json($placedOrderGraph['values']),
				borderWidth: 5,
				borderColor: '#6777ef',
				backgroundColor: 'transparent',
				pointBackgroundColor: '#fff',
				pointBorderColor: '#6777ef',
				pointRadius: 4
			}]
		},
		options: {
			legend: {
				display: false
			},
			scales: {
				yAxes: [{
					gridLines: {
						display: false,
						drawBorder: false,
					},
					ticks: {
						stepSize: {{ $placedOrderGraph['step'] }}
					}
				}],
				xAxes: [{
					gridLines: {
						color: '#fbfbfb',
						lineWidth: 2
					}
				}]
			},
		}
	});
</script>
@endpush