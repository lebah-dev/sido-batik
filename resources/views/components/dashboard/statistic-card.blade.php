<div class="row">

	<div class="col-lg-3 col-md-6 col-sm-6 col-12">
		<div class="card card-statistic-1">
			<div class="card-icon bg-danger"><i class="fas fa-star"></i></div>
			<div class="card-wrap">
				<div class="card-header"><h4>Pesanan Baru</h4></div>
				<div class="card-body">{{ $data['new_order'] ?? 0 }}</div>
			</div>
		</div>
	</div>

	<div class="col-lg-3 col-md-6 col-sm-6 col-12">
		<div class="card card-statistic-1">
			<div class="card-icon bg-warning"><i class="fas fa-paint-brush"></i></div>
			<div class="card-wrap">
				<div class="card-header"><h4>Pesanan Berjalan</h4></div>
				<div class="card-body">{{ $data['ongoing_order'] ?? 0 }}</div>
			</div>
		</div>
	</div>

	<div class="col-lg-3 col-md-6 col-sm-6 col-12">
		<div class="card card-statistic-1">
			<div class="card-icon bg-success"><i class="fas fa-check"></i></div>
			<div class="card-wrap">
				<div class="card-header"><h4>Pesanan Selesai</h4></div>
				<div class="card-body">{{ $data['finished_order'] ?? 0 }}</div>
			</div>
		</div>
	</div>      
	
	@if (auth()->user()->role == 'admin')
	<div class="col-lg-3 col-md-6 col-sm-6 col-12">
		<div class="card card-statistic-1">
			<div class="card-icon bg-primary"><i class="far fa-user"></i></div>
			<div class="card-wrap">
				<div class="card-header"><h4>Total Resellers</h4></div>
				<div class="card-body">{{ $data['total_reseller'] ?? 0 }}</div>
			</div>
		</div>
	</div>
	@else
	<div class="col-lg-3 col-md-6 col-sm-6 col-12">
		<div class="card card-statistic-1">
			<div class="card-icon bg-primary"><i class="fas fa-money-check"></i></div>
			<div class="card-wrap">
				<div class="card-header"><h4>Pesanan dapat diklaim</h4></div>
				<div class="card-body">{{ $data['claimable_order'] ?? 0 }}</div>
			</div>
		</div>
	</div>
	@endif
	
</div>

<div class="row">

	<div class="col-lg-3 col-md-6 col-sm-6 col-12">
		<div class="card card-statistic-1">
			<div class="card-icon bg-secondary"><i class="fas fa-user-clock"></i></div>
			<div class="card-wrap">
				<div class="card-header"><h4>Belum DP</h4></div>
				<div class="card-body">{{ $data['belum_dp'] ?? 0 }}</div>
			</div>
		</div>
	</div>

	<div class="col-lg-3 col-md-6 col-sm-6 col-12">
		<div class="card card-statistic-1">
			<div class="card-icon bg-info"><i class="fas fa-check"></i></div>
			<div class="card-wrap">
				<div class="card-header"><h4>Sudah DP</h4></div>
				<div class="card-body">{{ $data['sudah_dp'] ?? 0 }}</div>
			</div>
		</div>
	</div>

	<div class="col-lg-3 col-md-6 col-sm-6 col-12">
		<div class="card card-statistic-1">
			<div class="card-icon bg-secondary"><i class="fas fa-user-clock"></i></div>
			<div class="card-wrap">
				<div class="card-header"><h4>Belum Lunas</h4></div>
				<div class="card-body">{{ $data['belum_lunas'] ?? 0 }}</div>
			</div>
		</div>
	</div>      
	
	<div class="col-lg-3 col-md-6 col-sm-6 col-12">
		<div class="card card-statistic-1">
			<div class="card-icon bg-info"><i class="fas fa-check"></i></div>
			<div class="card-wrap">
				<div class="card-header"><h4>Sudah Lunas</h4></div>
				<div class="card-body">{{ $data['sudah_lunas'] ?? 0 }}</div>
			</div>
		</div>
	</div>
	
</div>