<div class="card">
	<form action="{{ route('profile.update-profile') }}" method="post" enctype="multipart/form-data">@csrf
		<div class="card-header">
			<h4 class="card-title">Profile</h4>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group row">
						<label class="col-lg-4 col-form-label">Full Name</label>
						<div class="col">
							<input type="text" name="full_name" value="{{ optional($user->profile)->full_name }}" class="form-control @error('full_name') is-invalid @enderror">
							@error('full_name') <div class="invalid-feedback">{{ $message }}</div> @enderror
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-4 col-form-label">Identity Number (KTP)</label>
						<div class="col">
							<input type="text" name="identity_number" value="{{ optional($user->profile)->identity_number }}" class="form-control @error('identity_number') is-invalid @enderror">
							@error('identity_number') <div class="invalid-feedback">{{ $message }}</div> @enderror
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-4 col-form-label">WA Number</label>
						<div class="col">
							<input type="text" name="wa_number" value="{{ optional($user->profile)->wa_number }}" class="form-control @error('wa_number') is-invalid @enderror">
							@error('wa_number') <div class="invalid-feedback">{{ $message }}</div> @enderror
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-4 col-form-label">Facebook Account</label>
						<div class="col">
							<input type="text" name="facebook_account" value="{{ optional($user->profile)->facebook_account }}" class="form-control @error('facebook_account') is-invalid @enderror">
							@error('facebook_account') <div class="invalid-feedback">{{ $message }}</div> @enderror
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-4 col-form-label">Portrait Photo</label>
						<div class="col">
							<input type="file" name="portrait_photo" class="form-control-file @error('portrait_photo') is-invalid @enderror">
							@error('portrait_photo') <div class="invalid-feedback">{{ $message }}</div> @enderror
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-4 col-form-label">Identity Scan</label>
						<div class="col">
							<input type="file" name="identity_scan" class="form-control-file @error('identity_scan') is-invalid @enderror">
							@error('identity_scan') <div class="invalid-feedback">{{ $message }}</div> @enderror
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-4">Verified At</label>
						<div class="col" title="{{ optional($user->profile)->verified_at }}">
							{{ optional(optional($user->profile)->verified_at)->diffForHumans() ?? 'Unverified' }}
						</div>
					</div>
					@if (optional($user->profile)->verified_at)
					<div class="form-group row">
						<label class="col-lg-4">Code Reference</label>
						<div class="col">
							<h4>{{ optional($user->profile)->code_reference ?? 'N/A' }}</h4>
						</div>
					</div>
					@if (optional($user->profile)->code_reference)
					<div class="form-group row">
						<label class="col-lg-4">Create Order URL</label>
						<div class="col">
							<a href="{{ route('place-order', ['code_reference' => optional($user->profile)->code_reference]) }}">
								{{ route('place-order', ['code_reference' => optional($user->profile)->code_reference]) }}
							</a>
						</div>
					</div>
					@endif
					@endif
					<div class="row">
						<div class="col offset-4">
							<button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Save Profile</button>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="mb-4">
						<img src="{{ optional($user->profile)->portrait_photo }}" alt="Portrait Photo" style="height: 200px;">
					</div>
					<div class="mb-4">
						<img src="{{ optional($user->profile)->identity_scan }}" alt="Identity Scan" style="height: 200px;">
					</div>
				</div>
			</div>
		</div>
	</form>
</div>