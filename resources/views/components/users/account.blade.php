<div class="card">
	<form id="verification-send" action="{{ route('verification.send') }}" method="post">@csrf</form>
	<form action="{{ route('profile.update-account') }}" method="post">
		@csrf @method('put')
		<div class="card-header">
			<h4 class="card-title">Edit User</h4>
		</div>
		<div class="card-body">
			<div class="form-group row">
				<label class="col-md-2 col-form-label">Role</label>
				<div class="col-md-3">
					<input type="text" name="role" value="{{ $user->role }}" class="form-control" readonly>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-2 col-form-label">Name</label>
				<div class="col-md-4">
					<input type="text" name="name" value="{{ $user->name }}" class="form-control @error('name') is-invalid @enderror">
					@error('name') <div class="invalid-feedback">{{ $message }}</div> @enderror
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-2 col-form-label">Email</label>
				<div class="col-md-4">
					<input type="email" name="email" value="{{ $user->email }}" class="form-control @error('email') is-invalid @enderror">
					@error('email') <div class="invalid-feedback">{{ $message }}</div> @enderror
					@if($user->email_verified_at == null) <div class="mt-1 small text-warning">Resend email verification link <a href="#verification-send" onclick="$('#verification-send').submit();">here</a></div> @endif
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-2 col-form-label">Password</label>
				<div class="col-md-5">
					<input type="password" name="password" class="form-control @error('password') is-invalid @enderror">
					<div class="text-small text-muted">Leave blank if don't want to change password</div>
					@error('password') <div class="invalid-feedback">{{ $message }}</div> @enderror
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-2 col-form-label">Password Confirmation</label>
				<div class="col-md-5">
					<input type="password" name="password_confirmation" class="form-control @error('password') is-invalid @enderror">
					<div class="text-small text-muted">Leave blank if don't want to change password</div>
				</div>
			</div>
			<div class="row">
				<div class="col offset-2">
					<button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Save Account Information</button>
				</div>
			</div>
		</div>
	</form>
</div>