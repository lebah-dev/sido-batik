<div class="card">
	<form action="{{ route('user-managements.bank-account', $user) }}" method="post">
		@csrf @method('put')
		<div class="card-header">
			<h4 class="card-title">Bank Account Detail</h4>
		</div>

		<div class="card-body">
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group row">
						<label class="col-lg-4 col-form-label">Bank Name</label>
						<div class="col">
							<input type="text" name="bank" value="{{ optional($user->bankAccount)->bank }}" class="form-control @error('bank') is-invalid @enderror">
							@error('bank') <div class="invalid-feedback">{{ $message }}</div> @enderror
						</div>
					</div>
		
					<div class="form-group row">
						<label class="col-lg-4 col-form-label">Account Name</label>
						<div class="col">
							<input type="text" name="name" value="{{ optional($user->bankAccount)->name }}" class="form-control @error('name') is-invalid @enderror">
							@error('name') <div class="invalid-feedback">{{ $message }}</div> @enderror
						</div>
					</div>
		
					<div class="form-group row">
						<label class="col-lg-4 col-form-label">Account Number</label>
						<div class="col">
							<input type="text" name="number" value="{{ optional($user->bankAccount)->number }}" class="form-control @error('number') is-invalid @enderror">
							@error('number') <div class="invalid-feedback">{{ $message }}</div> @enderror
						</div>
					</div>

					<div class="row">
						<div class="col offset-4">
							<button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Save Bank Information</button>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<p>Informasi bank ini akan kami gunakan untuk mengirim margin kepada anda. Pastikan data yang diisi sudah benar.</p>
				</div>
			</div>
		</div>
	</form>
</div>