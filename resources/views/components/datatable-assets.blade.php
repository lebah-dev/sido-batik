<x-slot name="cssLibs">
	<link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
</x-slot>
@push('scripts')
	<script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
	<script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>
	{{ isset($dataTable) ? $dataTable->scripts() : null }}
@endpush