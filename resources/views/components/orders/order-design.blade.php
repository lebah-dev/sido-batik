<div class="card">
	<div class="card-header">
		<h4 class="card-title">Desain Pesanan</h4>
		@if (auth()->check() && auth()->user()->isAdmin() && $order->designs()->count() <= 0)
			<div class="card-header-action">
				<button id="btn-submit-design" type="button" class="btn btn-success btn-icon icon-left" data-toggle="modal" data-target="#sumbit-design"><i class="fas fa-upload"></i> Submit Design</button>
			</div>
		@endif
	</div>
	<table class="table">
		@forelse ($order->designs->sortBy('version') as $design)
			<tr>
				<td class="text-left align-text-top text-nowrap" style="width: 1px;">{{ $design->created_at->diffForHumans() }}</td>
				<td class="text-left align-text-top pt-2 pb-4">
					<div>
						<a href="{{ $design->file }}">design-v{{ $design->version }}</a>
					</div>
					@if ($revision = $design->revision)
						<div><b>Revisi</b> : {{ $revision->text }}</div>
						<ul>
							@foreach ($revision->attachments as $attachment)
								<li>
									<div><a href="{{ $attachment->file }}">Attachment-{{ $attachment->id }}</a></div>
									<div>{{ $attachment->description }}</div>
								</li>
							@endforeach
							<li><a href="#add-revise-attachment-{{ $design->revision->id }}" class="text-secondary" data-toggle="modal">Tambah Attachment</a></li>
						</ul>
						@if (auth()->check() && auth()->user()->isAdmin() && !$revision->designRevision && !$order->designs()->approve()->exists())
							<button class="btn btn-sm btn-primary btn-icon icon-left" data-toggle="modal" data-target="#sumbit-design"><i class="fas fa-upload"></i> Submit Design</button>
						@endif
					@endif
				</td>
				<td class="text-right align-text-top">
					@if ($design->status == 'review')
						<button class="btn btn-sm btn-success btn-icon icon-left" data-confirm="{{ __('modal.approve-confirmation') }}" data-confirm-yes="$('#approve-design-{{ $design->id }}').submit();"><i class="fas fa-check"></i> Approve</button>
						@if ($order->designs()->reject()->count() < Sidobatik::call()->get('min_design_revise'))
						<button id="btn-revise-design" class="btn btn-sm btn-danger btn-icon icon-left" data-toggle="modal" data-target="#revise-design-{{ $design->id }}"><i class="fas fa-times"></i> Revisi</button>
						@endif
						
						<form id="approve-design-{{ $design->id }}" action="{{ route('order-managements.approve-design', [$order, $design])}}" method="post">
							@csrf @method('put')
						</form>
					@else
						<span class="badge badge-secondary">{{ $design->status }}</span>
					@endif
				</td>
			</tr>

			<x-modal id="revise-design-{{ $design->id }}" title="Revise Design">
				<form action="{{ route('order-managements.revise-design', [$order, $design]) }}" method="post" enctype="multipart/form-data">@csrf
					<div class="form-group">
						<label>Notes</label>
						<input type="text" name="text" class="form-control @error('text') is-invalid @enderror">
						@error('text')<div class="invalid-feedback">{{ $message }}</div>@enderror
					</div>
					<div class="form-group">
						<label>Attachment (.zip, .jpg, .png, .pdf)</label>
						<input type="file" name="attachment" class="form-control @error('attachment') is-invalid @enderror">
						@error('attachment')<div class="invalid-feedback">{{ $message }}</div>@enderror
					</div>
					<div class="form-group text-center">
						<button type="submit" class="btn btn-success btn-icon icon-left"><i class="fas fa-upload"></i> Sumbit</button>
					</div>
				</form>
			</x-modal>

			@if ($revision = $design->revision)
				<x-modal id="add-revise-attachment-{{ $revision->id }}" title="Revise Attachment">
					<form action="{{ route('order-managements.revise-design.store-attachment', [$order, $design, $revision]) }}" method="post" enctype="multipart/form-data">@csrf
						<div class="form-group">
							<label>Attachment (.zip, .jpg, .png, .pdf)</label>
							<input type="file" name="attachment" class="form-control @error('attachment') is-invalid @enderror">
							@error('attachment')<div class="invalid-feedback">{{ $message }}</div>@enderror
						</div>
						<div class="form-group">
							<label>Deskripsi</label>
							<textarea name="description" class="form-control @error('text') is-invalid @enderror"></textarea>
							@error('text')<div class="invalid-feedback">{{ $message }}</div>@enderror
						</div>
						<div class="form-group text-center">
							<button type="submit" class="btn btn-success btn-icon icon-left"><i class="fas fa-upload"></i> Sumbit</button>
						</div>
					</form>
				</x-modal>
			@endif
		@empty
			<tr>
				<td>
					<h4 class="my-5 text-center text-muted">Dalam Proses ...</h4>
				</td>
			</tr>
		@endforelse
	</table>
</div>

<x-modal id="sumbit-design" title="Submit Design">
	<form action="{{ route('order-managements.store-design', $order) }}" method="post" enctype="multipart/form-data">@csrf
		<div class="form-group">
			<label>File (.zip, .pdf, .jpg, .jpeg, .png)</label>
			<input type="file" name="file" class="form-control-file @error('file') is-invalid @enderror">
			@error('file')<div class="invalid-feedback">{{ $message }}</div>@enderror
		</div>
		<div class="form-group text-center">
			<button type="submit" class="btn btn-success btn-icon icon-left"><i class="fas fa-upload"></i> Sumbit</button>
		</div>
	</form>
</x-modal>

@error('file')
	@push('scripts')
		<script>
			$(document).ready(() => {
				$('#btn-submit-design').click()
			})
		</script>
	@endpush
@enderror