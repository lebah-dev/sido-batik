<div class="card bg-info">
	<div class="card-body">
		@switch($status ?? '')
			@case('new')
				<h3 class="text-center mb-4"><i class="fas fa-star"></i></h3>
				<h5 class="text-center m-0">Pesanan Baru</h5>
				@break
			@case('design')
				<h3 class="text-center mb-4"><i class="fas fa-draw-polygon" style="font-size: inherit;"></i></h3>
				<h5 class="text-center m-0">Proses Desain</h5>
				@break
			@case('down-payment')
				<h3 class="text-center mb-4"><i class="fas fa-credit-card" style="font-size: inherit;"></i></h3>
				<h5 class="text-center m-0">Pembayaran DP</h5>
				@break
			@case('production')
				<h3 class="text-center mb-4"><i class="fas fa-flag" style="font-size: inherit;"></i></h3>
				<h5 class="text-center m-0">Produksi</h5>
				@break
			@case('painting')
				<h3 class="text-center mb-4"><i class="fas fa-paint-brush" style="font-size: inherit;"></i></h3>
				<h5 class="text-center m-0">Pewarnaan</h5>
				@break
			@case('final-payment')
				<h3 class="text-center mb-4"><i class="fas fa-credit-card" style="font-size: inherit;"></i></h3>
				<h5 class="text-center m-0">Pelunasan</h5>
				@break
			@case('packaging')
				<h3 class="text-center mb-4"><i class="fas fa-boxes" style="font-size: inherit;"></i></h3>
				<h5 class="text-center m-0">Pemaketan</h5>
				@break
			@case('deliver')
				<h3 class="text-center mb-4"><i class="fas fa-truck-moving" style="font-size: inherit;"></i></h3>
				<h5 class="text-center m-0">Pengiriman</h5>
				@break
			@case('finished')
				<h3 class="text-center mb-4"><i class="fas fa-smile" style="font-size: inherit;"></i></h3>
				<h5 class="text-center m-0">Selesai</h5>
				@break
			@default
				<h3 class="text-center mb-4"><i class="fas fa-info-circle"></i></h3>
				<h5 class="text-center m-0">{{ $status }}</h5>
		@endswitch
	</div>
</div>