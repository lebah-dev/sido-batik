<div class="mb-4">
	@switch($order->status)
		@case('new')
			@if(auth()->user()->isAdmin())
				<button class="btn btn-block btn-icon icon-left btn-success" data-confirm="{{ __('modal.process-order-confirmation') }}" data-confirm-yes="$('#process-{{ $order->id }}').submit();"><i class="fas fa-check"></i> Proses</button>
			@endif
			
			<button onclick="location.href = '{{ route('order-managements.index') }}'" class="btn btn-block btn-icon icon-left btn-secondary"><i class="fas fa-chevron-left"></i> Kembali</button>
			<button onclick="location.href = '{{ route('order-managements.edit', $order->id) }}'" class="btn btn-block btn-icon icon-left btn-warning"><i class="fas fa-edit"></i> Edit</button>
			
			@if(auth()->user()->isAdmin())
				<button class="btn btn-block btn-icon icon-left btn-danger" data-confirm="{{ __('modal.archive-confirmation') }}" data-confirm-yes="$('#delete-{{ $order->id }}').submit();"><i class="fas fa-archive"></i> Arsipkan</button>
			@endif
			@break

		@case('design')
			@if (auth()->user()->isAdmin() && $order->designs()->approve()->exists())
				<button class="btn btn-block btn-icon icon-left btn-success" data-confirm="{{ __('modal.process-order-confirmation') }}" data-confirm-yes="$('#process-{{ $order->id }}').submit();"><i class="fas fa-check"></i> Proses</button>
			@endif
			<button onclick="location.href = '{{ route('order-managements.index') }}'" class="btn btn-block btn-icon icon-left btn-secondary"><i class="fas fa-chevron-left"></i> Kembali</button>
			@break

		@case('down-payment')
			@if (auth()->user()->isAdmin() && $order->total_price != null)
				<button class="btn btn-block btn-icon icon-left btn-success" data-confirm="{{ __('modal.process-order-confirmation') }}" data-confirm-yes="$('#process-{{ $order->id }}').submit();"><i class="fas fa-check"></i> Proses</button>
			@endif
			<button onclick="location.href = '{{ route('order-managements.index') }}'" class="btn btn-block btn-icon icon-left btn-secondary"><i class="fas fa-chevron-left"></i> Kembali</button>
			@break

		@case('production')
		@case('painting')
			@if(auth()->user()->isAdmin())
				<button class="btn btn-block btn-icon icon-left btn-success" data-confirm="{{ __('modal.process-order-confirmation') }}" data-confirm-yes="$('#process-{{ $order->id }}').submit();"><i class="fas fa-check"></i> Proses</button>
			@endif
			<button onclick="location.href = '{{ route('order-managements.index') }}'" class="btn btn-block btn-icon icon-left btn-secondary"><i class="fas fa-chevron-left"></i> Kembali</button>
			@break

		@case('final-payment')
			@if (auth()->user()->isAdmin() && $order->delivery_fee != null)
				<button class="btn btn-block btn-icon icon-left btn-success" data-confirm="{{ __('modal.process-order-confirmation') }}" data-confirm-yes="$('#process-{{ $order->id }}').submit();"><i class="fas fa-check"></i> Proses</button>
			@endif
			<button onclick="location.href = '{{ route('order-managements.index') }}'" class="btn btn-block btn-icon icon-left btn-secondary"><i class="fas fa-chevron-left"></i> Kembali</button>
			@break

		@case('packaging')
			@if (auth()->user()->isAdmin() && $order->shipping_code != null)
				<button class="btn btn-block btn-icon icon-left btn-success" data-confirm="{{ __('modal.process-order-confirmation') }}" data-confirm-yes="$('#process-{{ $order->id }}').submit();"><i class="fas fa-check"></i> Proses</button>
			@endif
			<button onclick="location.href = '{{ route('order-managements.index') }}'" class="btn btn-block btn-icon icon-left btn-secondary"><i class="fas fa-chevron-left"></i> Kembali</button>
			@break

		@case('deliver')
			@if(auth()->user()->isAdmin())
				<button class="btn btn-block btn-icon icon-left btn-success" data-confirm="{{ __('modal.process-order-confirmation') }}" data-confirm-yes="$('#process-{{ $order->id }}').submit();"><i class="fas fa-check"></i> Proses</button>
			@endif
			<button onclick="location.href = '{{ route('order-managements.index') }}'" class="btn btn-block btn-icon icon-left btn-secondary"><i class="fas fa-chevron-left"></i> Kembali</button>
			@break

		@default
			<button onclick="location.href = '{{ route('order-managements.index') }}'" class="btn btn-block btn-icon icon-left btn-secondary"><i class="fas fa-chevron-left"></i> Kembali</button>
			@break
	@endswitch

	<form id="process-{{ $order->id }}" action="{{ route('order-managements.process', $order->id) }}" method="post" class="hidden">@csrf @method('put')</form>
	<form id="delete-{{ $order->id }}" action="{{ route('order-managements.archive', $order->id) }}" method="post" class="hidden">@csrf @method('delete')</form>
</div>