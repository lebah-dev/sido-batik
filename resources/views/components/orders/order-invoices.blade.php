@push('styles')
<style>
	a.card:hover {
		text-decoration: none;
		box-shadow: 0 4px 8px rgb(0 0 0 / 9%);
	}
</style>
@endpush

<div>
	<h5>Faktur</h5>
	<hr>

	<form action="{{ route('order-invoice.create', $order) }}" method="post">@csrf
		@if($invoice = $order->getPendingDownPaymentInvoice())
		<a href="{{ route('order-invoice.show', $invoice) }}" target="_blank" class="card {{ $invoice->status == 'paid' ? 'card-success' : 'card-info' }}">
			<div class="card-body">
				<div class="small text-muted">{{ __($invoice->type) }}</div>
				<h6 class="mb-4 {{ $invoice->status == 'paid' ? 'text-success' : 'text-info' }}"><b>{{ $invoice->number }}</b></h6>
				<h5 class="mb-0 text-right text-muted">Rp. {{ number_format($invoice->amount) }}.-</h5>
				@if($invoice->status == 'paid')
					<div class="small text-right text-success">Dibayar pada: {{ $invoice->paid_at->format('d M Y H:i') }}</div>
				@elseif($invoice->status == 'pending')
					<div class="small text-right text-warning" title="{{ $invoice->due_date->format('d M Y H:i') }}">Jatuh tempo {{ $invoice->due_date->diffForHumans() }}</div>
				@endif
			</div>
		</a>
		@else
		<button type="submit" name="type" value="down-payment" class="mb-2 btn btn-block btn-primary btn-icon icon-left"><i class="fas fa-file-invoice"></i> Faktur Uang Muka</button>
		@endif

		@if($invoice = $order->getPendingFinalPaymentInvoice())
		<a href="{{ route('order-invoice.show', $invoice) }}" target="_blank" class="card {{ $invoice->status == 'paid' ? 'card-success' : 'card-info' }}">
			<div class="card-body">
				<div class="small text-muted">{{ __($invoice->type) }}</div>
				<h6 class="mb-4 {{ $invoice->status == 'paid' ? 'text-success' : 'text-info' }}"><b>{{ $invoice->number }}</b></h6>
				<h5 class="mb-0 text-right text-muted">Rp. {{ number_format($invoice->amount) }}.-</h5>
				@if($invoice->status == 'paid')
					<div class="small text-right text-success">Dibayar pada: {{ $invoice->paid_at->format('d M Y H:i') }}</div>
				@elseif($invoice->status == 'pending')
					<div class="small text-right text-warning" title="{{ $invoice->due_date->format('d M Y H:i') }}">Jatuh tempo {{ $invoice->due_date->diffForHumans() }}</div>
				@endif
			</div>
		</a>
		@elseif($order->passProduction() && $order->delivery_fee != null)
		<button type="submit" name="type" value="final-payment" class="mb-2 btn btn-block btn-primary btn-icon icon-left"><i class="fas fa-file-invoice"></i> Faktur Pelunasan</button>
		@endif
	</form>
</div>