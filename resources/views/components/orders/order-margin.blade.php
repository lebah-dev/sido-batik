<div class="card">
	<div class="card-header">
		<h4 class="card-title">Margin Pesanan</h4>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col">
				<form action="{{ route('order-managements.claim-margin', $order) }}" method="post">
					@csrf @method('put')
					<div class="row mb-2">
						<div class="col-lg-4 text-muted">Reseller</div>
						<div class="col"><a href="{{ route('user-managements.show', $order->reseller) }}">{{ $order->reseller->name }}</a></div>
					</div>
					<div class="row mb-4">
						<div class="col-lg-4 text-muted">Data diklaim</div>
						<div class="col">Rp. {{ number_format($order->quantity * Sidobatik::config('reseller_margin')) }}.-</div>
					</div>
					<div class="row">
						<div class="col-lg-4"></div>
						<div class="col">
							@if($order->reseller_claimed_at)
							<span class="badge badge-success">Diklaim pada {{ $order->reseller_claimed_at->format('d M Y H:i') }}</span>
							@else
								@if(auth()->user()->isAdmin())
								<button type="submit" class="btn btn-info">Tandai telah diklaim</button>
								@endif
							@endif
						</div>
					</div>
				</form>
			</div>
			<div class="col">
				<h5>Ketentuan Margin</h5>
				Setiap Reseller dan Agent berhak mendapat komisi berupa margin dari jumlah pesanan yang telah selesai. Margin akan segera dikirim ke rekening yang terdaftar di sistem. Pastikan nomor rekening anda benar.
			</div>
		</div>
	</div>
</div>