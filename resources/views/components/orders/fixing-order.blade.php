@inject('pricingRepository', 'App\Repositories\PricingRepository')

<div class="card">
	<div class="card-header">
		<h4 class="card-title">Fix Jumlah Pesanan</h4>
	</div>
	<div class="card-body pb-2">
		<div class="row">
			<div class="col">
				<form action="{{ route('order-managements.fix-order', $order) }}" method="post">
					@csrf @method('put')
					<div class="form-group row">
						<label class="col-lg-4 col-form-label">Ukuran Kain</label>
						<div class="col">
							<select name="size" class="form-control" {{ $order->status != 'down-payment' ? 'readonly' : '' }}>
								<option value="">-- Pilih Ukuran Kain --</option>
								@foreach ($pricingRepository->getSizeOptions() as $size)
								<option value="{{ $size->width . ' x ' . $size->length }}" {{ $order->size == ($size->width . ' x ' . $size->length) ? 'selected' : '' }}>{{ $size ->width . 'cm x ' . $size->length . 'cm' }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-4 col-form-label">Jumlah</label>
						<div class="col">
							<input type="number" name="quantity" class="form-control" value="{{ $order->quantity }}" {{ $order->status != 'down-payment' ? 'readonly' : '' }}>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-4 col-form-label">Catatan (optional)</label>
						<div class="col">
							<textarea name="notes" class="form-control" {{ $order->status != 'down-payment' ? 'readonly' : '' }}>{{ $order->notes }}</textarea>
						</div>
					</div>
					@if ($order->status == 'down-payment')
					<div class="form-group row">
						<div class="col-lg-4"></div>
						<div class="col">
							<button class="btn btn-success"><i class="fas fa-save"></i> Simpan Jumlah Pesanan</button>
						</div>
					</div>
					@endif
					@if ($order->total_price != null)
					<hr>
					<div class="form-group row">
						<label class="col-lg-4">Total Harga</label>
						<div class="col">
							<h4 id="total-price">Rp. {{ number_format($order->total_price, 2) }}</h4>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-lg-4">Uang Muka (50%)</label>
						<div class="col">
							<div id="down-payment">Rp. {{ number_format($order->total_price * 0.5, 2) }}</div>
						</div>
					</div>
					@endif
				</form>
			</div>
			<div class="col">
				<h5>Ketentuan Pembayaran</h5>
				Pembayaran dapat dilakukan dengan transfer ke rekening yang tertera pada faktur pembayaran. Faktur pembayaran data dilihat pada bagian "Faktur" di halaman ini. Nominal transfer akan dijumlah dengan kode unik pembayaran, pastikan melakukan transfer sesuai dengan nominal yang tertera.
			</div>
		</div>
	</div>
</div>