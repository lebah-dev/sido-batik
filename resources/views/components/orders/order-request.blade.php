<div class="card">
	<div class="card-header">
		<h4 class="card-title">Deskripsi Permintaan</h4>
	</div>
	<div class="card-body">
		<div class="mb-4">{!! $order->request_description !!}</div>
		<div class="mb-2 font-weight-bold">Contoh Gambar</div>
		<div class="row">
			@foreach ($order->images as $image)
				<div class="col-lg-12 mb-3">
					<div class="row">
						<div class="col-lg-3">
							<img class="img-thumbnail" src="{{ $image->image }}" alt="" style="max-height: 10rem; max-width: 100%">
						</div>
						<div class="col">
							<div class="font-weight-bold">Description :</div>
							<div class="mb-4">{{ $image->description }}</div>
							<div class="small"><a href="#delete-image-{{ $image->id }}" class="text-danger" data-confirm="{{ __('modal.delete-confirmation') }}" data-confirm-yes="$('#delete-image-{{ $image->id }}').submit();"><i class="fas fa-trash"></i> Delete</a></div>
							<form id="delete-image-{{ $image->id }}" action="{{ route('order-managements.delete-image', [$order, $image]) }}" method="POST">@csrf @method('delete')</form>
						</div>
					</div>
				</div>
			@endforeach
			@if (!$order->passPayment())
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-3">
						<button class="btn btn-block btn-sm btn-outline-success" data-toggle="modal" data-target="#add-image"><i class="fas fa-plus"></i> Add Image</button>
					</div>
				</div>
			</div>
			@endif
		</div>
	</div>
</div>

<x-modal id="add-image" title="Add Image">
	<form action="{{ route('order-managements.store-image', $order) }}" method="post" enctype="multipart/form-data">@csrf
		<div class="form-group">
				<label>Image Example</label>
				<input type="file" name="image" class="form-control-file @error('image') is-invalid @enderror">
				@error('image') <div class="invalid-feedback">{{ $message }}</div> @enderror
		</div>
		<div class="form-group">
			<label>Image Description</label>
			<textarea name="image_description" class="form-control @error('image_description') is-invalid @enderror"></textarea>
			@error('image_description') <div class="invalid_feedback">{{ $message }}</div> @enderror
		</div>
		<div class="form-group text-center">
			<button class="btn btn-success btn-icon icon-left"><i class="fas fa-plus"></i> Add Image</button>
		</div>
	</form>
</x-modal>