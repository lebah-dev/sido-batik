<div class="card">
	<div class="card-header">
		<h4 class="card-title">Reseller</h4>
	</div>
	<div class="card-body">
		<div class="row mb-2">
			<div class="col-12 text-muted">Nama Lengkap</div>
			<div class="col-12">{{ $order->reseller->profile->full_name }}</div>
		</div>
		<div class="row mb-2">
			<div class="col-12 text-muted">Nomor WA</div>
			<div class="col-12">{{ $order->reseller->profile->wa_number }}</div>
		</div>
		<div class="row mb-2">
			<div class="col-12 text-muted">Email</div>
			<div class="col-12">{{ $order->reseller->email }}</div>
		</div>
		<div class="row mb-2">
			<div class="col-12">
				<img class="img-thumbnail" src="{{ $order->reseller->profile->portrait_photo }}" alt="Portrait Photo">
			</div>
		</div>
	</div>
</div>