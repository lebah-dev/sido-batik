<div class="card">
	<div class="card-header">
		<h4 class="card-title">Resi Pengiriman</h4>
	</div>
	<div class="card-body">
		<form action="{{ route('order-managements.shipping-code', $order) }}" method="post">
			@csrf @method('put')
			<div class="form-group row">
				<label class="col-lg-2">Kurir</label>
				<div class="col">
					{{ $order->courier_name }} - {{ $order->courier_service }} ({{ $order->courier_estimated_delivery }}) - Rp. {{ (int) $order->delivery_fee }}
				</div>
			</div>
			<div class="form-group row">
				<label class="col-lg-2 col-form-label">Resi</label>
				<div class="col-lg-6">
					<input type="text" name="shipping_code" value="{{ $order->shipping_code }}" class="form-control @error('shipping_code') is-invalid @enderror" {{ !auth()->check() || (auth()->check() && !auth()->user()->isAdmin()) || ($order->status != 'packaging') ? 'readonly' : '' }}>
				</div>
			</div>
			@auth()
			@if(auth()->user()->isAdmin() && $order->status == 'packaging')
			<div class="row">
				<div class="col-lg-2"></div>
				<div class="col">
					<button type="submit" class="btn btn-success btn-icon icon-left"><i class="fas fa-save"></i> Simpan Resi</button>
				</div>
			</div>
			@endif
			@endauth
		</form>
	</div>
</div>