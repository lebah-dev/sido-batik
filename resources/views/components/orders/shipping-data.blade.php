<div class="card">
	<div class="card-header">
		<h4 class="card-title">Data Pesanan</h4>
	</div>
	<div class="card-body">
		<form action="{{ route('order-managements.shipping-data', $order) }}" method="post">
			@csrf @method('put')
			<div class="form-group row">
				<label class="col-lg-2 co-form-label">Nama Peneriam</label>
				<div class="col-lg-6">
					<input type="text" name="receiver_name" value="{{ $order->receiver_name }}" class="form-control @error('receiver_name') is-invalid @enderror" {{ $order->status != 'final-payment' ? 'readonly' : '' }}>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-lg-2 co-form-label">No. Telp Penerima</label>
				<div class="col-lg-4">
					<input type="text" name="receiver_phone" value="{{ $order->receiver_phone }}" class="form-control @error('receiver_phone') is-invalid @enderror" {{ $order->status != 'final-payment' ? 'readonly' : '' }}>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-lg-2 col-form-label">Provinsi</label>
				<div class="col-lg-6">
					<select id="select-province" name="shipping_province" class="form-control @error('shipping_province') is-invalid @enderror" {{ $order->status != 'final-payment' ? 'disabled' : '' }}>
						@if($order->shipping_province)
						<option value="{{ $order->shipping_province }}" selected="selected">{{ $order->shipping_province_name }}</option>
						@endif
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-lg-2 col-form-label">Kabupaten / Kota</label>
				<div class="col-lg-6">
					<select id="select-city" name="shipping_city" class="form-control @error('shipping_city') is-invalid @enderror" {{ $order->shipping_province == null || $order->status != 'final-payment' ? 'disabled' : ''}}>
						@if($order->shipping_city)
						<option value="{{ $order->shipping_city }}" selected="selected">{{ $order->shipping_city_name }}</option>
						@endif
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-lg-2 col-form-label">Alamat Lengkap</label>
				<div class="col">
					<textarea name="shipping_address" class="form-control @error('shipping_address') is-invalid @enderror" rows="10" {{ $order->status != 'final-payment' ? 'readonly' : '' }}>{{ $order->shipping_address }}</textarea>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-lg-2">Daftar Kurir</label>
				<div id="courier-packages" class="col">
					@if($order->courier_name)
					<div><label><input type="radio" name="courier_package" value="{{ $order->courier_name }}|{{ $order->courier_service }}|{{ $order->courier_estimated_delivery }}|{{ $order->delivery_fee }}" checked> {{ $order->courier_name }} - {{ $order->courier_service }} ({{ $order->courier_estimated_delivery }}) - Rp. {{ (int) $order->delivery_fee }}</label></div>
					@else
					<div>memuat ...</div>
					@endif
				</div>
			</div>
			<div class="form-group row">
				<div class="col-lg-2"></div>
				<div class="col">
					@if($order->status == 'final-payment')
					<button type="submit" class="btn btn-success btn-icon icon-left"><i class="fas fa-save"></i> Simpan </button>
					@endif
				</div>
			</div>
			<hr>
			@if ($order->delivery_fee)
			<div class="form-group row">
				<label class="col-lg-2">Biaya Pengirman</label>
				<div class="col">
					<div>Rp. {{ number_format($order->delivery_fee, 2) }}</div>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-lg-2">Total Pelunasan</label>
				<div class="col">
					<h4>Rp. {{ number_format((($order->total_price * 0.5) + $order->delivery_fee), 2) }}</h4>
				</div>
			</div>
			@endif
		</form>
		<div class="small">
			<strong>Note: </strong> Semua produk akan dikirim dari  
			<strong>{{ ucwords(str_replace('-', ' ', explode('_', Sidobatik::config('warehouse_city'))[1])) }}</strong>, 
			<strong>{{ ucwords(str_replace('-', ' ', explode('_', Sidobatik::config('warehouse_province'))[1])) }}</strong>
		</div>
	</div>
</div>

@push('scripts')
<script>
	setTimeout(() => {
		$('#select-province').select2({
			ajax: {
				url: '{{ route('api.raja-ongkir.province') }}',
				dataType: 'json',
				cache: true,
			}
		})

		$('#select-city').select2({
			ajax: {
				url: '{{ route('api.raja-ongkir.city') }}',
				dataType: 'json',
				data: params => {
					return {
						q: params.term,
						province: $('#select-province').val().toString().split('_')[0]
					}
				},
				cache: true,
			}
		})

		$('#select-province').change(e => {
			if (e.target.value != null || e.target.value != '') {
				$('#select-city').prop('disabled', false)
				$('#select-city').val('')
			} else {
				$('#select-city').prop('disabled', true)
			}
		})

		$('#select-city').change(e => {
			if (e.target.value != null || e.target.value != '') {
				$.post('{{ route('api.raja-ongkir.cost') }}', {
					destination: $('#select-province').val().toString().split('_')[0],
					weight: {{ $order->weight * $order->quantity }}
				}, (data, status) => {
					if (status == 'success') {
						let radiobuttons = ''
						data.results.forEach(courier => {
							courier.costs.forEach(service => {
								service.cost.forEach(cost => {
									radiobuttons += '<div><label><input type="radio" name="courier_package" value="'+courier.name+'|'+service.service+'|'+cost.etd+'|'+cost.value+'" onchange="courierPackageChecked(this)"> '+courier.name+' - '+service.service+' ('+cost.etd+') - Rp. '+cost.value+'</label></div>'
								})
							})
						})
						$('#courier-packages').html(radiobuttons)
					}
				})
			}
		})
	}, 300)

	function courierPackageChecked(el) {
		$('#delivery-cost').text(formatMoney($(el).val().toString().split('|')[3]))
	}
</script>
@endpush