<div class="text-nowrap">
	<a href="{{ route('order-managements.show', $id) }}" class="text-info"><i class="fas fa-eye"></i></a>
	@if($status == 'new')
	<a href="{{ route('order-managements.edit', $id) }}" class="text-warning"><i class="fas fa-edit"></i></a>
	@endif
</div>