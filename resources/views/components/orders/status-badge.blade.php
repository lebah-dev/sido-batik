@switch($status)
	@case('new')
		<span class="small text-info font-weight-bold text-uppercase">{{ __($status) }}</span>
		@break
	@default
		<span class="small text-info font-weight-bold text-uppercase">{{ __($status) }}</span>
@endswitch