<div class="card">
	<div class="card-header">
		<h4 class="card-title">Bukti Terima Pesanan</h4>
	</div>
	<div class="card-body">
		<form action="{{ route('order-managements.upload-delivery-receipt', $order) }}" method="post" enctype="multipart/form-data">
			@csrf
			<div class="form-group">
				<label>Upload bukti terima barang (dapat berupa foto dalam format .jpg atau .png)</label>
				<div class="input-group mb-3">
					<input type="file" name="delivery_receipt" class="form-control @error('delivery_receipt') is-invalid @enderror" {{ $order->status == 'deliver' ? '' : 'disabled' }}>
					<div class="input-group-append">
						<button class="btn btn-primary btn-icon icon-left" type="submit" {{ $order->status == 'deliver' ? '' : 'disabled' }}><i class="fas fa-file-upload"></i> Upload</button>
					</div>
				</div>
				@error('delivery_receipt')<div class="invalid-feedback">{{ $message }}</div>@enderror
			</div>
			@if ($order->delivery_receipt_path)
			<div class="text-center">
				<img src="{{ $order->delivery_receipt }}" alt="Delivery Receipt" style="width: 200px;">
			</div>
			@endif
		</form>
	</div>
</div>