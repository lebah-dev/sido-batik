<div class="card">
	<div class="card-header">
		<h4 class="card-title">Pelanggan</h4>
	</div>
	<div class="card-body">
		<div class="row mb-2">
			<div class="col-12 text-muted">Nama Lengkap</div>
			<div class="col-12">{{ $order->customer }}</div>
		</div>
		<div class="row mb-2">
			<div class="col-12 text-muted">Nomor WA</div>
			<div class="col-12">{{ $order->wa_number }}</div>
		</div>
		<div class="row mb-2">
			<div class="col-12 text-muted">Email</div>
			<div class="col-12">{{ $order->email }}</div>
		</div>
		<div class="row mb-2">
			<div class="col-12 text-muted">Alamat</div>
			<div class="col-12">{{ $order->address }}</div>
		</div>
	</div>
</div>