<div class="card">
	<div class="card-header">
		<h4 class="card-title">Detail Pesanan</h4>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col">
				<div class="form-group row">
					<div class="col-md-4">Dibuat Pada</div>
					<div class="col">{{ $order->created_at->isoFormat('dddd, DD MMMM YYYY HH:mm') }}</div>
				</div>
				<div class="form-group row">
					<div class="col-md-4">Nomor</div>
					<div class="col">{{ $order->number }}</div>
				</div>
				<div class="form-group row">
					<div class="col-md-4">Kode Reseller</div>
					<div class="col">{{ $order->reseller->profile->code_reference }}</div>
				</div>
			</div>
			<div class="col-md-4">
				<x-orders.status-card :status="$order->status"></x-orders.status-card>
			</div>
		</div>
	</div>
</div>