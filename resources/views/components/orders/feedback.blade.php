<div class="card card-primary">
	<div class="card-body">
		<h3 class="mb-4">Terimakasih Atas Pesanan Anda</h3>
		<p>Bantu kami untuk selalu meningkatkan kualitas layanan kami dengan mengisi formulir di bawah ini.</p>
		<a href="{{ Sidobatik::call()->get('feedback_google_form_link') }}" target="_blank">Sidobatik Feedback Form</a>
	</div>
</div>