@push('modals')
<div class="modal fade" tabindex="-1" role="dialog" id="{{ $id ?? 'modal-dialog' }}">
	<div class="modal-dialog {{ $width ?? '' == 'big' ? 'modal-lg' : '' }}" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">{{ $title ?? 'Modal Title' }}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body pb-0">
				{{ $slot }}
			</div>
		</div>
	</div>
</div>
@endpush