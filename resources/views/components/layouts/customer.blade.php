<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>{{ $header ?? 'Home' }} &mdash; {{ config('app.name') }}</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('assets/modules/fontawesome/css/all.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/modules/bootstrap/css/bootstrap.min.css') }}">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{ asset('assets/modules/bootstrap-social/bootstrap-social.css') }}">
  {{ $cssLibs ?? null }}

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">

  <style>
    body.layout-3 .main-content {
      padding-left: 0;
      padding-right: 0;
      padding-top: 80px;
    }

    @media (max-width: 1024px) {
      .main-content {
        padding-top: 65px;
        padding-right: 0px;
        padding-left: 0px;
      }
    }

  </style>

  <!-- Page Specific Style -->
  @stack('styles')
</head>

<body class="layout-3">
  <div id="app">
    <div class="main-wrapper container">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <a href="{{ route('home') }}" class="navbar-brand sidebar-gone-hide">
          <img src="{{ asset('assets/img/logo.png') }}" alt="{{ config('app.name') }}" height="75" class="py-2" style="filter: invert(100%);">
        </a>
        <div class="nav-collapse">
          <a class="sidebar-gone-show nav-collapse-toggle nav-link" href="#">
            <i class="fas fa-ellipsis-v"></i>
          </a>
          <ul class="navbar-nav">
            <li class="nav-item"><a href="{{ route('place-order') }}" class="nav-link">Place Order</a></li>
            <li class="nav-item"><a href="{{ route('track-order') }}" class="nav-link">Track Order</a></li>
          </ul>
        </div>
      </nav>

      <!-- Main Content -->
      <div class="main-content">
        {{ $slot }}
      </div>

      <footer class="main-footer">
        <div class="footer-left">
          Copyright &copy; 2021 <div class="bullet"></div> {{ config('app.name') }}</a>
        </div>
        <div class="footer-right"></div>
      </footer>
    </div>

    @stack('modals')
  </div>

  <!-- General JS Scripts -->
  <script src="{{ asset('assets/modules/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/modules/popper.js') }}"></script>
  <script src="{{ asset('assets/modules/tooltip.js') }}"></script>
  <script src="{{ asset('assets/modules/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
  <script src="{{ asset('assets/modules/moment.min.js') }}"></script>
  <script src="{{ asset('assets/js/stisla.js') }}"></script>

  <!-- JS Libraies -->
  {{ $jsLibs ?? null }}

  <!-- Page Specific JS File -->
  @stack('scripts')

  <!-- Template JS File -->
  <script src="{{ asset('assets/js/scripts.js') }}"></script>
  <script src="{{ asset('assets/js/custom.js') }}"></script>

</body>

</html>
