@if($message = session('info'))
<div class="alert alert-info alert-has-icon alert-dismissible fade show">
	<div class="alert-icon"><i class="far fa-lightbulb"></i></div>
	<div class="alert-body">
		<button class="close" data-dismiss="alert">
			<span>×</span>
		</button>
		<div class="alert-title">Info</div>
		{{ $message }}
	</div>
</div>
@endif

@if($message = session('status'))
<div class="alert alert-info alert-has-icon alert-dismissible fade show">
	<div class="alert-icon"><i class="far fa-lightbulb"></i></div>
	<div class="alert-body">
		<button class="close" data-dismiss="alert">
			<span>×</span>
		</button>
		<div class="alert-title">Info</div>
		{{ __('status.' . $message) }}
	</div>
</div>
@endif