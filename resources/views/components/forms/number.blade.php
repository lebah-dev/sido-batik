<div class="form-group row">
	<label for="{{ $key }}" class="col-md-2 col-form-label">{{ $title ?? ucwords($key) }}</label>
	
	<div class="col-md-4">
		<div class="input-group">
			@isset($prepend)
			<div class="input-group-prepend">
				<span class="input-group-text">{{ $prepend }}</span>
			</div>
			@endisset

			<input type="number" name="{{ $key }}" id="{{ $key }}" class="form-control" value="{{ $value ?? old($key) ?? 0 }}" placeholder="{{ $title }}" {{ isset($readonly) ? 'readonly' : '' }} step="{{ $step ?? 1 }}">
			
			@isset($append)
			<div class="input-group-append">
				<span class="input-group-text">{{ $append }}</span>
			</div>
			@endisset
		</div>
		
		@isset($help) <div class="mt-1 small text-info">{{ $help }}</div> @endisset
		@error($key) <div class="mt-1 small text-danger">{{ $message }}</div> @enderror
	</div>
</div>