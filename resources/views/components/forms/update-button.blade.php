<div class="form-group row">
	<div class="col-md-2"></div>
	<div class="col">
		<button class="btn btn-success btn-icon icon-left"><i class="fas fa-save"></i> Update Data</button>
		<a href="{{ $back ?? '#' }}" class="btn btn-secondary btn-icon icon-left"><i class="fas fa-chevron-left"></i> Back</a>
	</div>
</div>