<div class="form-group row mb-0">
	<label for="{{ $key ?? '' }}" class="col-md-2 col-form-label">{{ $title ?? ucwords($key ?? '') }}</label>
	<div class="col-md-6">
		@isset($value)
			<div class="form-control font-weight-bold border-0">{{ $value }}</div>
		@else
			<div class="mb-4">{{ $slot }}</div>
		@endif
	</div>
</div>