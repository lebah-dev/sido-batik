<div class="form-group row">
	<label for="{{ $key }}" class="col-md-2 col-form-label">{{ $title ?? ucwords($key) }}</label>
	<div class="col-md-6">
		<input type="password" name="{{ $key }}" id="{{ $key }}" class="form-control" placeholder="{{ $title }}">
		<input type="password" name="{{ $key }}_confirmation" id="{{ $key }}_confirmation" class="form-control mt-2" placeholder="Konfirmasi {{ $title }}">
		@error($key) <div class="mt-1 small text-danger">{{ $message }}</div> @enderror
	</div>
</div>