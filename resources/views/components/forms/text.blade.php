<div class="form-group row">
	<label for="{{ $key }}" class="col-md-2 col-form-label">{{ $title ?? ucwords($key) }}</label>
	<div class="col-md-6">
		<input type="text" name="{{ $key }}" id="{{ $key }}" class="form-control" value="{{ $value ?? old($key) }}" placeholder="{{ $title }}" {{ isset($readonly) ? 'readonly' : '' }} {{ isset($options) ? 'list=' . $key . '-list' : '' }} autocomplete="none">
		@isset($options)
			<datalist id="{{ $key }}-list">
				@foreach($options as $option)
					<option value="{{ $option }}"></option>
				@endforeach
			</datalist>
		@endisset
		@error($key) <div class="mt-1 small text-danger">{{ $message }}</div> @enderror
	</div>
</div>