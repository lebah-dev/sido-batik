<div class="form-group row">
	<label for="{{ $key }}" class="col-md-2 col-form-label">{{ $title ?? ucwords($key) }}</label>
	<div class="col-md-6">
		<select name="{{ $key }}" id="{{ $key }}" class="form-control" {{ isset($readonly) ? 'readonly' : '' }}>
			<option value="">-- Choose {{ $title }} --</option>
			@foreach($options as $option => $text)
			<option value="{{ $option }}" {{ !isset($value) ? (old($key) == $option ? 'selected' : '') : ($value == $option ? 'selected' : '')  }}>{{ $text }}</option>
			@endforeach
		</select>
		@error($key) <div class="mt-1 small text-danger">{{ $message }}</div> @enderror
	</div>
</div>