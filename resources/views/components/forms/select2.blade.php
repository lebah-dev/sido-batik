@push('styles')
<link rel="stylesheet" href="{{ asset('stisla/modules/select2/dist/css/select2.min.css') }}">
@endpush

<div class="form-group row">
	<label for="{{ $key }}" class="col-md-2 col-form-label">{{ $title ?? ucwords($key) }}</label>
	<div class="col-md-6">
		<select name="{{ $key }}" id="{{ $key }}" class="form-control select2" {{ isset($readonly) ? 'readonly' : '' }} {{ isset($multiple) ? 'multiple' : '' }}>
			<option value="">-- Choose {{ $title }} --</option>
			@foreach($options as $option => $text)
			<option value="{{ $option }}" {{ !isset($value) ? (old($key) == $option ? 'selected' : '') : ($value == $option ? 'selected' : '')  }} {{ isset($values) ? (in_array($option, $values) ? 'selected' : '') : '' }}>{{ $text }}</option>
			@endforeach
		</select>
		@error($key) <div class="mt-1 small text-danger">{{ $message }}</div> @enderror
	</div>
</div>

@push('scripts')
<script src="{{ asset('stisla/modules/select2/dist/js/select2.full.min.js') }}"></script>
@endpush