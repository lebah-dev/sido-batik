<div class="form-group row">
	<label for="{{ $key }}" class="col-md-2 col-form-label">{{ $title ?? ucwords($key) }}</label>
	<div class="col-md-6">
		<textarea name="{{ $key }}" id="{{ $key }}" class="form-control" rows="4" placeholder="{{ $title }}" {{ isset($readonly) ? 'readonly' : '' }}>{{ $value ?? old($key) }}</textarea>
		@error($key) <div class="mt-1 small text-danger">{{ $message }}</div> @enderror
	</div>
</div>