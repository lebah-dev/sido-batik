<div id="main-sidebar" class="main-sidebar sidebar-style-2">
	<aside id="sidebar-wrapper">
		<div class="sidebar-brand">
			<a href="{{ route('home') }}">
				<img src="{{ asset('assets/img/logo.png') }}" alt="{{ config('app.name') }}" height="75" class="py-2">
			</a>
		</div>
		<div class="sidebar-brand sidebar-brand-sm">
			<a href="{{ route('home') }}">SB</a>
		</div>
		<ul class="sidebar-menu">
			<li class="menu-header">Menu</li>
			<li class="{{ request()->routeIs('dashboard') ? 'active' : '' }}"><a class="nav-link" href="{{ route('dashboard')}}"><i class="fas fa-tachometer-alt"></i> <span>Dashboard</span></a></li>

			@if(auth()->user()->isAdmin())
			<li class="{{ request()->routeIs('user-managements*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('user-managements.index')}}"><i class="fas fa-users"></i> <span>Manajemen User</span></a></li>
			@endif
			
			<li class="{{ request()->routeIs('order-managements*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('order-managements.index')}}"><i class="fas fa-receipt"></i> <span>Manajemen Pesanan</span></a></li>
			
			@if(auth()->user()->isAdmin())
			<li class="{{ request()->routeIs('transfer-margin*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('transfer-margin.index')}}"><i class="fas fa-file-invoice-dollar"></i> <span>Transfer Margin</span></a></li>
			@endif
			
			@if(!auth()->user()->isAdmin())
			<li class="{{ request()->routeIs('margin-claims*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('margin-claims.index')}}"><i class="fas fa-file-invoice-dollar"></i> <span>Klaim Margin</span></a></li>
			@endif

			<li class="{{ request()->routeIs('marketing-materials*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('marketing-materials.index')}}"><i class="fas fa-book-reader"></i> <span>Materi Marketing</span></a></li>

			@if(auth()->user()->isAdmin())
			<li class="dropdown {{ (request()->routeIs('configurations*') || request()->routeIs('pricings*')) ? 'active' : '' }}">
				<a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-toolbox"></i><span>Konfigurasi</span></a>
				<ul class="dropdown-menu">
					<li class="{{ request()->routeIs('pricings*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('pricings.index')}}">Harga</a></li>
					<li class="{{ request()->routeIs('configurations*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('configurations')}}">Aplikasi</a></li>
				</ul>
			</li>
			@endif

		</ul>
	</aside>
</div>