<x-layouts.customer>
	<div class="row">
		<div class="col-lg-12 mb-4">
			<div class="hero text-white hero-bg-image hero-bg-parallax" style="background-image: url('assets/img/unsplash/andre-benz-1214056-unsplash.jpg');">
				<div class="hero-inner">
					<h1>Welcome to {{ config('app.name') }}</h1>
					<p class="lead mt-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ab autem dolorem nobis a, quasi nesciunt dolores, voluptatum corporis sed, quam harum quod fugit odio. Dolore consectetur molestias excepturi necessitatibus quam.</p>
					<div class="mt-3">
						<a href="#" class="btn btn-outline-white btn-lg btn-icon icon-left"><i class="far fa-user"></i> Place Order</a>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-12 mb-4">
			<div class="card">
				<div class="card-header">
					<h4>What make us Different</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-8">
							<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime possimus, aspernatur qui veritatis corrupti voluptate molestiae beatae sint illum repellat maiores impedit non ducimus, perferendis ab alias asperiores velit magnam cupiditate sequi. Eos itaque recusandae autem nesciunt, voluptatibus fugit laboriosam sapiente pariatur facere, quasi quibusdam cum magnam velit consequatur reiciendis?</p>
							<div class="gallery gallery-md">
								<div class="gallery-item" data-image="{{ asset('assets/img/news/img03.jpg') }}" data-title="Image 1" href="{{ asset('assets/img/news/img03.jpg') }}" title="Image 1" style="background-image: url(&quot;assets/img/news/img03.jpg&quot;);"></div>
								<div class="gallery-item" data-image="{{ asset('assets/img/news/img08.jpg') }}" data-title="Image 3" href="{{ asset('assets/img/news/img08.jpg') }}" title="Image 3" style="background-image: url(&quot;assets/img/news/img08.jpg&quot;);"></div>
								<div class="gallery-item" data-image="{{ asset('assets/img/news/img11.jpg') }}" data-title="Image 5" href="{{ asset('assets/img/news/img11.jpg') }}" title="Image 5" style="background-image: url(&quot;assets/img/news/img11.jpg&quot;);"></div>
								<div class="gallery-item" data-image="{{ asset('assets/img/news/img12.jpg') }}" data-title="Image 8" href="{{ asset('assets/img/news/img12.jpg') }}" title="Image 8" style="background-image: url(&quot;assets/img/news/img12.jpg&quot;);"></div>
								<div class="gallery-item" data-image="{{ asset('assets/img/news/img13.jpg') }}" data-title="Image 9" href="{{ asset('assets/img/news/img13.jpg') }}" title="Image 9" style="background-image: url(&quot;assets/img/news/img13.jpg&quot;);"></div>
								<div class="gallery-item" data-image="{{ asset('assets/img/news/img15.jpg') }}" data-title="Image 11" href="{{ asset('assets/img/news/img15.jpg') }}" title="Image 11" style="background-image: url(&quot;assets/img/news/img15.jpg&quot;);"></div>
								<div class="gallery-item gallery-more" data-image="{{ asset('assets/img/news/img08.jpg') }}" data-title="Image 12" href="{{ asset('assets/img/news/img08.jpg') }}" title="Image 12" style="background-image: url(&quot;assets/img/news/img08.jpg&quot;);">
									<div>+2</div>
								</div>
								<div class="gallery-item gallery-hide" data-image="{{ asset('assets/img/news/img01.jpg') }}" data-title="Image 9" href="{{ asset('assets/img/news/img01.jpg') }}" title="Image 9" style="background-image: url(&quot;assets/img/news/img01.jpg&quot;);"></div>
							</div>
						</div>
						<div class="col-lg-4">
							<div id="carouselExampleIndicators3" class="carousel slide" data-ride="carousel">
								<ol class="carousel-indicators">
									<li data-target="#carouselExampleIndicators3" data-slide-to="0" class=""></li>
									<li data-target="#carouselExampleIndicators3" data-slide-to="1" class=""></li>
									<li data-target="#carouselExampleIndicators3" data-slide-to="2" class="active"></li>
								</ol>
								<div class="carousel-inner">
									<div class="carousel-item">
										<img class="d-block w-100" src="{{ asset('assets/img/news/img01.jpg') }}" alt="First slide">
									</div>
									<div class="carousel-item">
										<img class="d-block w-100" src="{{ asset('assets/img/news/img07.jpg') }}" alt="Second slide">
									</div>
									<div class="carousel-item active">
										<img class="d-block w-100" src="{{ asset('assets/img/news/img08.jpg') }}" alt="Third slide">
									</div>
								</div>
								<a class="carousel-control-prev" href="#carouselExampleIndicators3" role="button" data-slide="prev">
									<span class="carousel-control-prev-icon" aria-hidden="true"></span>
									<span class="sr-only">Previous</span>
								</a>
								<a class="carousel-control-next" href="#carouselExampleIndicators3" role="button" data-slide="next">
									<span class="carousel-control-next-icon" aria-hidden="true"></span>
									<span class="sr-only">Next</span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-12 mb-4">
			<div class="row">
				<div class="col-12 col-md-4 col-lg-4">
					<div class="pricing">
						<div class="pricing-title">
							Developer
						</div>
						<div class="pricing-padding">
							<div class="pricing-price">
								<div>$19</div>
								<div>per month</div>
							</div>
							<div class="pricing-details">
								<div class="pricing-item">
									<div class="pricing-item-icon"><i class="fas fa-check"></i></div>
									<div class="pricing-item-label">1 user agent</div>
								</div>
								<div class="pricing-item">
									<div class="pricing-item-icon"><i class="fas fa-check"></i></div>
									<div class="pricing-item-label">Core features</div>
								</div>
								<div class="pricing-item">
									<div class="pricing-item-icon"><i class="fas fa-check"></i></div>
									<div class="pricing-item-label">1GB storage</div>
								</div>
								<div class="pricing-item">
									<div class="pricing-item-icon"><i class="fas fa-check"></i></div>
									<div class="pricing-item-label">2 Custom domain</div>
								</div>
								<div class="pricing-item">
									<div class="pricing-item-icon bg-danger text-white"><i class="fas fa-times"></i></div>
									<div class="pricing-item-label">Live Support</div>
								</div>
							</div>
						</div>
						<div class="pricing-cta">
							<a href="#">Subscribe <i class="fas fa-arrow-right"></i></a>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4 col-lg-4">
					<div class="pricing pricing-highlight">
						<div class="pricing-title">
							Small Team
						</div>
						<div class="pricing-padding">
							<div class="pricing-price">
								<div>$60</div>
								<div>per month</div>
							</div>
							<div class="pricing-details">
								<div class="pricing-item">
									<div class="pricing-item-icon"><i class="fas fa-check"></i></div>
									<div class="pricing-item-label">5 user agent</div>
								</div>
								<div class="pricing-item">
									<div class="pricing-item-icon"><i class="fas fa-check"></i></div>
									<div class="pricing-item-label">Core features</div>
								</div>
								<div class="pricing-item">
									<div class="pricing-item-icon"><i class="fas fa-check"></i></div>
									<div class="pricing-item-label">10GB storage</div>
								</div>
								<div class="pricing-item">
									<div class="pricing-item-icon"><i class="fas fa-check"></i></div>
									<div class="pricing-item-label">10 Custom domain</div>
								</div>
								<div class="pricing-item">
									<div class="pricing-item-icon"><i class="fas fa-check"></i></div>
									<div class="pricing-item-label">24/7 Support</div>
								</div>
							</div>
						</div>
						<div class="pricing-cta">
							<a href="#">Subscribe <i class="fas fa-arrow-right"></i></a>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4 col-lg-4">
					<div class="pricing">
						<div class="pricing-title">
							Enterprise
						</div>
						<div class="pricing-padding">
							<div class="pricing-price">
								<div>$499</div>
								<div>per month</div>
							</div>
							<div class="pricing-details">
								<div class="pricing-item">
									<div class="pricing-item-icon"><i class="fas fa-check"></i></div>
									<div class="pricing-item-label">Unlimited user agent</div>
								</div>
								<div class="pricing-item">
									<div class="pricing-item-icon"><i class="fas fa-check"></i></div>
									<div class="pricing-item-label">Core features</div>
								</div>
								<div class="pricing-item">
									<div class="pricing-item-icon"><i class="fas fa-check"></i></div>
									<div class="pricing-item-label">8TB storage</div>
								</div>
								<div class="pricing-item">
									<div class="pricing-item-icon"><i class="fas fa-check"></i></div>
									<div class="pricing-item-label">Unlimited custom domain</div>
								</div>
								<div class="pricing-item">
									<div class="pricing-item-icon"><i class="fas fa-check"></i></div>
									<div class="pricing-item-label">Lifetime Support</div>
								</div>
							</div>
						</div>
						<div class="pricing-cta">
							<a href="#">Subscribe <i class="fas fa-arrow-right"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-12 mb-4">
			<div class="card card-primary">
				<div class="row m-0">
					<div class="col-12 col-md-12 col-lg-5 p-0">
						<div class="card-header text-center"><h4>Contact Us</h4></div>
						<div class="card-body">
							<form method="POST">
								<div class="form-group floating-addon">
									<label>Name</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<div class="input-group-text">
												<i class="far fa-user"></i>
											</div>
										</div>
										<input id="name" type="text" class="form-control" name="name" placeholder="Name">
									</div>
								</div>

								<div class="form-group floating-addon">
									<label>Email</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<div class="input-group-text">
												<i class="fas fa-envelope"></i>
											</div>
										</div>
										<input id="email" type="email" class="form-control" name="email" placeholder="Email">
									</div>
								</div>

								<div class="form-group">
									<label>Message</label>
									<textarea class="form-control" placeholder="Type your message" data-height="150" style="height: 150px;"></textarea>
								</div>

								<div class="form-group text-right">
									<button type="submit" class="btn btn-round btn-lg btn-primary">
										Send Message
									</button>
								</div>
							</form>
						</div>  
					</div>
					<div class="col-12 col-md-12 col-lg-7 p-0">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2795.153444124746!2d111.00810023374505!3d-7.795763700318067!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a28d06d87bf0b%3A0x329b8a0037e275c6!2sSIDOBATIK!5e0!3m2!1sen!2sid!4v1614870282299!5m2!1sen!2sid" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>

	@push('scripts')
		{{-- <script src="http://maps.google.com/maps/api/js?key="></script> --}}
		<script src="{{ asset('assets/modules/gmaps.js') }}"></script>

		<script>
			// initialize map
			var map = new GMaps({
				div: '#map',
				lat: -6.5637928,
				lng: 106.7535061,
				zoomControl: false,
				fullscreenControl: false,
				mapTypeControl: true,
				mapTypeControlOptions: {
					mapTypeIds: []
				}
			});
			// Added a overlay to the map
			map.drawOverlay({
				lat: -6.5637928,
				lng: 106.7535061,
				content: `
					<div class="popover" style="width:250px;">
						<div class="manual-arrow"><i class="fas fa-caret-down"></i></div>
						<div class="popover-body">
							<b>Multinity</b>
							<p><small>Jl. HM. Syarifudin, Bubulak, Bogor Bar., <br>Kota Bogor, Jawa Barat 16115</p>
							<p><a target="_blank" href="http://multinity.com">Website</a></small></p>
						</div>
					</div>`
			});
		</script>
	@endpush
</x-layouts.customer>