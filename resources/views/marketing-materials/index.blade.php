<x-app-layout>
	<x-slot name="header">Materi Marketing</x-slot>

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Index Materi Marketing</h4>
			@if(auth()->user()->isAdmin())
			<div class="card-header-action">
				<a href="{{ route('marketing-materials.create') }}" class="btn btn-success btn-icon icon-left"><i class="fas fa-plus"></i> Tambah Materi Marketing</a>
			</div>
			@endif
		</div>
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						@if(auth()->user()->isAdmin())<th></th>@endif
						<th>Gambar</th>
						<th>Judul</th>
						<th>Caption</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach ($marketingMaterials as $item)
						<tr>
							@if(auth()->user()->isAdmin())
							<td class="text-nowrap" style="width: 1px;">
								<a href="{{ route('marketing-materials.edit', $item) }}" class="text-warning"><i class="fas fa-edit"></i></a>
								<a href="#delete" onclick="$('#delete-{{ $item->id }}').submit();" class="text-danger"><i class="fas fa-trash"></i></a>
								<form action="{{ route('marketing-materials.destroy', $item) }}" id="delete-{{ $item->id }}" method="post">@csrf @method('delete')</form>
							</td>
							@endif
							<td style="width: 1px;">
								<img src="{{ $item->image }}" alt="" height="75">
							</td>
							<td>{{ $item->title }}</td>
							<td>{{ $item->caption }}</td>
							<td class="text-nowrap">
								<a href="{{ route('storage-download', encrypt($item->image_path)) }}" class="text-info"><i class="fas fa-file-download"></i> Download</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</x-app-layout>