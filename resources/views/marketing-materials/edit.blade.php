<x-app-layout>
	<x-slot name="header">Materi Marketing</x-slot>

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Edit Materi Marketing</h4>
		</div>
		<div class="card-body">
			<form action="{{ route('marketing-materials.update', $marketingMaterial) }}" method="post" enctype="multipart/form-data">
				@csrf @method('put')
				<x-forms.text key="title" title="Judul" value="{{ $marketingMaterial->title }}"></x-forms.text>
				<x-forms.file key="image" title="Gambar" value="{{ $marketingMaterial->image }}" help="Biarkan kosong jika tidak ingin mengubah gambar saat ini."></x-forms.file>
				<x-forms.detail key="image_path" title="">
					<img src="{{ $marketingMaterial->image }}" alt="" height="250">
				</x-forms.detail>
				<x-forms.textarea key="caption" title="Caption" value="{{ $marketingMaterial->caption }}"></x-forms.textarea>
				<x-forms.update-button back="{{ route('marketing-materials.index') }}"></x-forms.update-button>
			</form>
		</div>
	</div>
</x-app-layout>