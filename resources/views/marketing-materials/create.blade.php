<x-app-layout>
	<x-slot name="header">Materi Marketing</x-slot>

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Tambah Materi Marketing</h4>
		</div>
		<div class="card-body">
			<form action="{{ route('marketing-materials.store') }}" method="post" enctype="multipart/form-data">@csrf
				<x-forms.text key="title" title="Judul"></x-forms.text>
				<x-forms.file key="image" title="Gambar"></x-forms.file>
				<x-forms.textarea key="caption" title="Caption"></x-forms.textarea>
				<x-forms.create-button back="{{ route('marketing-materials.index') }}"></x-forms.create-button>
			</form>
		</div>
	</div>
</x-app-layout>