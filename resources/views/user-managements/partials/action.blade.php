<div class="text-nowrap">
	<a href="{{ route('user-managements.show', $id) }}" class="text-info"><i class="fas fa-eye"></i></a>
	<a href="{{ route('user-managements.edit', $id) }}" class="text-warning"><i class="fas fa-edit"></i></a>
	<form id="delete-{{ $id }}" action="{{ route('user-managements.delete', $id) }}" method="post" class="hidden">@csrf @method('delete')</form>
</div>