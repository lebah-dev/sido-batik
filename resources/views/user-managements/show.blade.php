<x-app-layout>
	<x-slot name="header">User's Detail</x-slot>

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Account Detail</h4>
			<div class="card-header-action text-nowrap">
				<a href="{{ route('user-managements.index') }}" class="btn btn-light"><i class="fas fa-chevron-left"></i> Back</a>
				<a href="{{ route('user-managements.edit', $user) }}" class="btn btn-warning"><i class="fas fa-edit"></i> Edit</a>
				<button class="btn btn-danger" data-confirm="{{ __('modal.delete-confirmation') }}" data-confirm-yes="$('#delete-{{ $user->id }}').submit();"><i class="fas fa-trash"></i> Delete</button>
			</div>
			<form id="delete-{{ $user->id }}" action="{{ route('user-managements.delete', $user) }}" method="post" class="hidden">@csrf @method('delete')</form>
		</div>
		<div class="card-body">
			<div class="form-group row">
				<label class="col-md-2 col-form-label">Role</label>
				<div class="col-md-3">
					<input type="text" value="{{ $user->role }}" class="form-control" readonly>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-2 col-form-label">Name</label>
				<div class="col-md-4">
					<input type="text" value="{{ $user->name }}" class="form-control" readonly>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-2 col-form-label">Email</label>
				<div class="col-md-4">
					<input type="text" value="{{ $user->email }}" class="form-control" readonly>
				</div>
			</div>
		</div>
	</div>

	<div class="card">
		<form id="verify" action="{{ route('user-managements.verify', $user) }}" method="post">@csrf @method('put')</form>
		<form action="{{ route('user-managements.update-profile', $user) }}" method="post" enctype="multipart/form-data">@csrf
			<div class="card-header">
				<h4 class="card-title">Profile</h4>
				<div class="card-header-action text-nowrap">
					@if ($user->profile && !$user->profile->verified_at) <button type="button" class="btn btn-info" data-confirm="{{ __('modal.verify-confirmation') }}" data-confirm-yes="$('#verify').submit();"><i class="fas fa-check"></i> Verify</button> @endif
					<button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Save</button>
				</div>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group row">
							<label class="col-lg-4 col-form-label">Full Name</label>
							<div class="col">
								<input type="text" name="full_name" value="{{ optional($user->profile)->full_name }}" class="form-control @error('full_name') is-invalid @enderror">
								@error('full_name') <div class="invalid-feedback">{{ $message }}</div> @enderror
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-4 col-form-label">Identity Number (KTP)</label>
							<div class="col">
								<input type="text" name="identity_number" value="{{ optional($user->profile)->identity_number }}" class="form-control @error('identity_number') is-invalid @enderror">
								@error('identity_number') <div class="invalid-feedback">{{ $message }}</div> @enderror
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-4 col-form-label">WA Number</label>
							<div class="col">
								<input type="text" name="wa_number" value="{{ optional($user->profile)->wa_number }}" class="form-control @error('wa_number') is-invalid @enderror">
								@error('wa_number') <div class="invalid-feedback">{{ $message }}</div> @enderror
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-4 col-form-label">Facebook Account</label>
							<div class="col">
								<input type="text" name="facebook_account" value="{{ optional($user->profile)->facebook_account }}" class="form-control @error('facebook_account') is-invalid @enderror">
								@error('facebook_account') <div class="invalid-feedback">{{ $message }}</div> @enderror
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-4 col-form-label">Portrait Photo</label>
							<div class="col">
								<input type="file" name="portrait_photo" class="form-control-file @error('portrait_photo') is-invalid @enderror">
								@error('portrait_photo') <div class="invalid-feedback">{{ $message }}</div> @enderror
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-4 col-form-label">Identity Scan</label>
							<div class="col">
								<input type="file" name="identity_scan" class="form-control-file @error('identity_scan') is-invalid @enderror">
								@error('identity_scan') <div class="invalid-feedback">{{ $message }}</div> @enderror
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-4">Verified At</label>
							<div class="col" title="{{ optional($user->profile)->verified_at }}">
								{{ optional(optional($user->profile)->verified_at)->diffForHumans() ?? 'Unverified' }}
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-4">Code Reference</label>
							<div class="col">
								<h4>{{ optional($user->profile)->code_reference ?? 'N/A' }}</h4>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-4">Create Order URL</label>
							<div class="col">
								<a href="{{ route('place-order', ['code_reference' => optional($user->profile)->code_reference]) }}">
									{{ route('place-order', ['code_reference' => optional($user->profile)->code_reference]) }}
								</a>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="mb-4">
							<img src="{{ optional($user->profile)->portrait_photo }}" alt="Portrait Photo" style="height: 200px;">
						</div>
						<div class="mb-4">
							<img src="{{ optional($user->profile)->identity_scan }}" alt="Identity Scan" style="height: 200px;">
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>

	<x-users.bank-account :user="$user"></x-users.bank-account>
</x-app-layout>