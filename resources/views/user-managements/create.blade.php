<x-app-layout>
	<x-slot name="header">Users Form</x-slot>

	<div class="row">
		<div class="col-12">
			<form action="{{ route('user-managements.store') }}" method="post">@csrf
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Create User</h4>
					</div>
					<div class="card-body">
						<div class="form-group row">
							<label class="col-md-2 col-form-label">Role</label>
							<div class="col-md-3">
								<select name="role" value="{{ old('role') }}" class="form-control @error('role') is-invalid @enderror">
									<option value="">-- Choose Role --</option>
									@foreach($roles as $role => $text)
									<option value="{{ $role }}" {{ old('role') == $role ? 'selected' : '' }}>{{ $text }}</option>
									@endforeach
								</select>
								@error('role') <div class="invalid-feedback">{{ $message }}</div> @enderror
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-2 col-form-label">Name</label>
							<div class="col-md-4">
								<input type="text" name="name" value="{{ old('name') }}" class="form-control @error('name') is-invalid @enderror">
								@error('name') <div class="invalid-feedback">{{ $message }}</div> @enderror
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-2 col-form-label">Email</label>
							<div class="col-md-4">
								<input type="email" name="email" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror">
								@error('email') <div class="invalid-feedback">{{ $message }}</div> @enderror
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-2 col-form-label">Password</label>
							<div class="col-md-5">
								<input type="password" name="password" class="form-control @error('password') is-invalid @enderror">
								@error('password') <div class="invalid-feedback">{{ $message }}</div> @enderror
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-2 col-form-label">Password Confirmation</label>
							<div class="col-md-5">
								<input type="password" name="password_confirmation" class="form-control @error('password') is-invalid @enderror">
							</div>
						</div>
					</div>
					<div class="card-footer text-right">
						<a href="{{ route('user-managements.index') }}" class="btn btn-light"><i class="fas fa-chevron-left"></i> Cancel</a>
						<button class="btn btn-success"><i class="fas fa-plus"></i> Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</x-app-layout>