<x-app-layout>
	<x-slot name="header">User Managements</x-slot>
	
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					{{ $dataTable->table() }}
				</div>
			</div>
		</div>
	</div>

	<x-datatable-assets :data-table="$dataTable"></x-datatable-assets>
</x-app-layout>