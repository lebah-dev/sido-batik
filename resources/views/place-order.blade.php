<x-layouts.customer>
	<div class="row">
		<div class="col-lg-12">
			<form action="{{ route('place-order') }}" method="post" enctype="multipart/form-data">@csrf
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Create Order</h4>
					</div>
					<div class="card-body">
						<div class="form-group row">
							<label class="col-md-2 col-form-label">Customer Name</label>
							<div class="col-md-4">
								<input type="text" name="customer" value="{{ old('customer') }}" class="form-control @error('customer') is-invalid @enderror">
								@error('customer') <div class="invalid-feedback">{{ $message }}</div> @enderror
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-2 col-form-label">Customer Address</label>
							<div class="col-md-8">
								<textarea name="address" class="form-control @error('address') is-invalid @enderror">{{ old('address') }}</textarea>
								@error('address') <div class="invalid-feedback">{{ $message }}</div> @enderror
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-2 col-form-label">WhatsApp Number</label>
							<div class="col-md-3">
								<input type="text" name="wa_number" value="{{ old('wa_number') }}" class="form-control @error('wa_number') is-invalid @enderror">
								@error('wa_number') <div class="invalid-feedback">{{ $message }}</div> @enderror
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-2 col-form-label">Email</label>
							<div class="col-md-4">
								<input type="email" name="email" value="{{ request('email') }}" class="form-control @error('reseller_id') is-invalid @enderror" {{ request('email') ? 'readonly' : ''}}>
								@error('email') <div class="invalid-feedback">{{ $message }}</div> @enderror
							</div>
						</div>
						<hr>
						<div class="form-group row">
							<label class="col-md-2 col-form-label">Request Description</label>
							<div class="col-md-8">
								<textarea name="request_description" class="summernote-simple form-control @error('request_description') is-invalid @enderror">{{ old('request_description') }}</textarea>
								@error('request_description') <div class="invalid-feedback">{{ $message }}</div> @enderror
							</div>
						</div>
						<hr>
						<div class="form-group row">
							<label class="col-md-2 col-form-label">Image Example</label>
							<div class="col-md-4">
								<input type="file" name="image" class="form-control-file @error('image') is-invalid @enderror">
								@error('image') <div class="invalid-feedback">{{ $message }}</div> @enderror
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-2 col-form-label">Image Description</label>
							<div class="col-md-6">
								<textarea name="image_description" class="form-control @error('image_description') is-invalid @enderror"></textarea>
								@error('image_description') <div class="invalid_feedback">{{ $message }}</div> @enderror
							</div>
						</div>
						<hr>
						<div class="form-group row">
							<label class="col-md-2 col-form-label">Reseller Code</label>
							<div class="col-md-4">
								<input type="text" name="code_reference" value="{{ request('code_reference') }}" class="form-control @error('reseller_id') is-invalid @enderror" {{ request('code_reference') ? 'readonly' : ''}}>
								@error('reseller_id') <div class="invalid-feedback">{{ $message }}</div> @enderror
							</div>
						</div>
					</div>
					<div class="card-footer text-right">
						<a href="{{ route('user-managements.index') }}" class="btn btn-light"><i class="fas fa-chevron-left"></i> Cancel</a>
						<button class="btn btn-success"><i class="fas fa-plus"></i> Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<x-slot name="cssLibs">
		<link rel="stylesheet" href="{{ asset('assets/modules/summernote/summernote-bs4.css') }}">
	</x-slot>
	<x-slot name="jsLibs">
		<script src="{{ asset('assets/modules/summernote/summernote-bs4.js') }}"></script>
	</x-slot>
</x-layouts.customer>