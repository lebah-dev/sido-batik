<x-app-layout>
	<x-slot name="header">Konfigurasi Harga</x-slot>

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Index Konfigurasi</h4>
			<div class="card-header-action">
				<a href="{{ route('pricings.create') }}" class="btn btn-success btn-icon icon-left"><i class="fas fa-plus"></i> Tambah</a>
			</div>
		</div>
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th></th>
						<th>Ukuran</th>
						<th>Berat</th>
						<th>Min Qty</th>
						<th>Harga Satuan</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($pricings as $pricing)
					<tr>
						<td class="text-nowrap" style="width: 1px;">
							<a href="{{ route('pricings.edit', $pricing) }}" class="text-warning"><i class="fas fa-edit"></i></a>
							<a href="#" class="text-danger" data-confirm="{{ __('modal.delete-confirmation') }}" data-confirm-yes="$('#delete-{{ $pricing->id }}').submit();"><i class="fas fa-trash"></i></a>
							<form id="delete-{{ $pricing->id }}" action="{{ route('pricings.destroy', $pricing) }}" method="post">@csrf @method('delete')</form>
						</td>
						<td>{{ $pricing->width . 'cm x ' . $pricing->length . 'cm'}}</td>
						<td>{{ $pricing->weight }} gr / lbr</td>
						<td>≥ {{ $pricing->min_qty }} lbr</td>
						<td>Rp. {{ number_format($pricing->unit_price, 2) }} / lbr</td>
					</tr>
					@empty
					<tr>
						<td colspan="3">
							<div class="text-center">Belum ada konfigurasi. <a href="{{ route('pricings.create') }}">Tambah baru.</a></div>
						</td>
					</tr>
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</x-app-layout>