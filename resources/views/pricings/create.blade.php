<x-app-layout>
	<x-slot name="header">Konfigurasi Harga</x-slot>

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Pengkondisian Harga</h4>
		</div>
		<div class="card-body">
			<form action="{{ route('pricings.store') }}" method="post">@csrf
				<x-forms.number key="width" title="Lebar" append="cm" value="{{ $pricing->width ?? null }}"></x-forms.number>
				<x-forms.number key="length" title="Panjang" append="cm" value="{{ $pricing->length ?? null }}"></x-forms.number>
				<x-forms.number key="weight" title="Berat" append="gr / sheet" value="{{ $pricing->weight ?? null }}"></x-forms.number>
				<x-forms.number key="min_qty" title="Min Qty" prepend="≥" append="sheet" value="{{ $pricing->min_qty ?? null }}"></x-forms.number>
				<x-forms.number key="unit_price" title="Harga Satuan" prepend="Rp" append=".00 / sheet" value="{{ $pricing->unit_price ?? null }}"></x-forms.number>
				<x-forms.update-button back="{{ route('pricings.index') }}"></x-forms.update-button>
			</form>
		</div>
	</div>
</x-app-layout>