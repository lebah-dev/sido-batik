<x-app-layout>
	<x-slot name="header">Dashboard</x-slot>

	<x-dashboard.statistic-card :data="$statistics"></x-dashboard.statistic-card>
	
	<x-dashboard.graph-card :new-orders="$newOrders" :placed-order-graph="$placedOrderGraph"></x-dashboard.graph-card>
</x-app-layout>