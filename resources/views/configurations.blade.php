<x-app-layout>
	<x-slot name="header">Konfigurasi</x-slot>

	@if($errors->any())
	<div class="card">
		<div class="card-body">
			<ul>
				@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	</div>
	@endif

	<form action="{{ route('configurations') }}" method="post">
		@csrf @method('put')
		<div class="card">
			<div class="card-header">
				<h4 class="card-title">Gudang</h4>
			</div>
			<div class="card-body">
				<div class="form-group row">
					<label class="col-lg-2 col-form-label">Provinsi</label>
					<div class="col-lg-6">
						<select id="select-province" name="warehouse_province" class="form-control @error('warehouse_province') is-invalid @enderror">
							@if($config->get('warehouse_province'))
							<option value="{{ $config->get('warehouse_province') }}">{{ ucwords(str_replace('-', ' ', explode('_', $config->get('warehouse_province'))[1])) }}</option>
							@endif
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-lg-2 col-form-label">Kabupaten / Kota</label>
					<div class="col-lg-6">
						<select id="select-city" name="warehouse_city" class="form-control @error('warehouse_city') is-invalid @enderror">
							@if($config->get('warehouse_city'))
							<option value="{{ $config->get('warehouse_city') }}">{{ ucwords(str_replace('-', ' ', explode('_', $config->get('warehouse_city'))[1])) }}</option>
							@endif
						</select>
					</div>
				</div>
			</div>
		</div>
	
		<div class="card">
			<div class="card-header">
				<h4 class="card-title">Pesanan</h4>
			</div>
			<div class="card-body">
				<x-forms.number key="reseller_margin" title="Reseller" value="{{ $config->get('reseller_margin') }}"></x-forms.number>

				<div class="form-group row">
					<label class="col-lg-2 col-form-label">Revisi Desain Minimal</label>
					<div class="col-lg-4">
						<input type="number" name="min_design_revise" value="{{ $config->get('min_design_revise') }}" class="form-control @error('min_design_revise') is-invalid @enderror">
						@error('min_design_revise') <div class="invalid-feedback">{{ $message }}</div> @enderror
					</div>
				</div>
	
				<div class="form-group row">
					<label class="col-lg-2 col-form-label">Batas Pembayaran Invoice</label>
					<div class="col-lg-4">
						<input type="number" name="invoice_due_days" value="{{ $config->get('invoice_due_days') }}" class="form-control @error('invoice_due_days') is-invalid @enderror">
						@error('invoice_due_days') <div class="invalid-feedback">{{ $message }}</div> @enderror
					</div>
				</div>
			</div>
		</div>

		<div class="card">
			<div class="card-header">
				<h4 class="card-title">Metode Transfer</h4>

				<div class="card-header-action">
					<button type="button" class="btn btn-info btn-icon icon-left" data-toggle="modal" data-target="#add-transfer-method"><i class="fas fa-plus"></i> Tambah Metode Transfer</button>
				</div>
			</div>
			<table class="table table-md">
				<thead>
					<tr>
						<th style="width: 1px;"></th>
						<th>Bank</th>
						<th>Atas Nama</th>
						<th>No. Rekening</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($transferMethods as $bank)
					<tr>
						<td class="text-nowrap">
							<button type="button" class="btn btn-sm btn-danger btn-icon" onclick="$('#delete-transfer-method-{{ $bank->id }}').submit();"><i class="fas fa-trash"></i></button>
						</td>
						<td>{{ $bank->bank }}</td>
						<td>{{ $bank->name }}</td>
						<td>{{ $bank->number }}</td>
					</tr>
					@empty
					<tr>
						<td colspan="4" class="text-center">belum ada metode transfer</td>
					</tr>
					@endforelse
				</tbody>
			</table>
		</div>

		<div class="card">
			<div class="card-header">
				<h4 class="card-title">Formulir Feedback</h4>
			</div>
			<div class="card-body">
				<div class="form-group row">
					<label class="col-lg-2 col-form-label">Link Google Form untuk Feedback</label>
					<div class="col-lg-8">
						<input type="text" name="feedback_google_form_link" value="{{ $config->get('feedback_google_form_link') }}" class="form-control @error('feedback_google_form_link') is-invalid @enderror">
						@error('feedback_google_form_link') <div class="invalid-feedback">{{ $message }}</div> @enderror
					</div>
				</div>
			</div>
		</div>

		<div class="d-flex justify-content-between">
			<div>
				<button type="button" class="btn btn-secondary btn-icon icon-left" onclick="$('#reset-config').submit();"><i class="fas fa-clock"></i> Reset ke Awal</button>
			</div>
			<div>
				<button type="submit" class="btn btn-success btn-icon icon-left"><i class="fas fa-save"></i> Simpan Konfigurasi</button>
			</div>
		</div>
	</form>
	<form action="{{ route('configurations') }}" id="reset-config" method="post">@csrf @method('delete')</form>
	@foreach ($transferMethods as $bank)
	<form action="{{ route('transfer-methods.delete', $bank) }}" id="delete-transfer-method-{{ $bank->id }}" method="post">@csrf @method('delete')</form>
	@endforeach

	<x-modal id="add-transfer-method" title="Add Transfer Method">
		<form action="{{ route('transfer-methods.store') }}" method="post">@csrf
			<div class="form-group row">
				<label class="col-lg-4 col-form-label">Nama Bank</label>
				<div class="col">
					<input type="text" name="bank" class="form-control @error('bank') is-invalid @enderror">
					@error('bank') <div class="invalid-feedback">{{ $message }}</div> @enderror
				</div>
			</div>

			<div class="form-group row">
				<label class="col-lg-4 col-form-label">Atas Nama</label>
				<div class="col">
					<input type="text" name="name" class="form-control @error('name') is-invalid @enderror">
					@error('name') <div class="invalid-feedback">{{ $message }}</div> @enderror
				</div>
			</div>

			<div class="form-group row">
				<label class="col-lg-4 col-form-label">No. Rekening</label>
				<div class="col">
					<input type="text" name="number" class="form-control @error('number') is-invalid @enderror">
					@error('number') <div class="invalid-feedback">{{ $message }}</div> @enderror
				</div>
			</div>

			<div class="form-group text-right">
				<button class="btn btn-success btn-icon icon-left"><i class="fas fa-save"></i> Tambah Metode Transfer</button>
			</div>
		</form>
	</x-modal>

	@push('styles')
		<link rel="stylesheet" href="{{ asset('assets/modules/select2/dist/css/select2.min.css') }}">
	@endpush

	@push('scripts')
	<script src="{{ asset('assets/modules/cleave-js/dist/cleave.min.js') }}"></script>
	<script src="{{ asset('assets/modules/select2/dist/js/select2.full.min.js') }}"></script>

	<script>
		setTimeout(() => {
			$('#select-province').select2({
				ajax: {
					url: '{{ route('api.raja-ongkir.province') }}',
					dataType: 'json',
					cache: true,
				}
			})

			$('#select-city').select2({
				ajax: {
					url: '{{ route('api.raja-ongkir.city') }}',
					dataType: 'json',
					data: params => {
						return {
							q: params.term,
							province: $('#select-province').val().toString().split('_')[0]
						}
					},
					cache: true,
				}
			})

			$('#select-province').change(e => {
				if (e.target.value != null || e.target.value != '') {
					$('#select-city').prop('disabled', false)
					$('#select-city').val('')
				} else {
					$('#select-city').prop('disabled', true)
				}
			})
		}, 300)
	</script>
	@endpush
</x-app-layout>