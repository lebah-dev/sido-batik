<?php

return [
    'success' => [
        'delete' => 'Data berhasil dihapus dari database',
        'store' => 'Data berhasil disimpan di database',
        'update' => 'Data berhasil diperbaharui dan disimpan di database',
    ],

    'error' => [
        'delete' => 'Terjadi kesalahan saat menghapus data',
        'store' => 'Terjadi kesalahan saat menyimpan data',
        'update' => 'Terjadi kesalahan saat memparbaharui data'
    ]
];