<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentications for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Data login tidak sesuai.',
    'password' => 'Passord tidak sesuai.',
    'throttle' => 'Batas percobaan login telah terlampaui. Silahkan coba lagi dalam :seconds detik.',

];
