<?php

return [
	'delete-confirmation' => 'Konfirmasi Penghapusan|Anda akan menghapus data dari database. Aksi ini tidak dapat dikembalikan. Apakah anda ingin melanjutkan?',

	'process-order-confirmation' => 'Konfirmasi Proses Order|Anda akan memproses pesanan ke tahap selanjutnya. Aksi ini tidak dapat dikembalikan. Apakah anda ingin melanjutkan?',
	'archive-confirmation' => 'Konfrimasi Pengarsipan|Anda akan mengarsipkan data. Aksi ini tidak dapat dikembalikan. Apakah anda ingin melanjutkan?',
	'approve-confirmation' => 'Konfirmasi Persetujuan|Anda akan menyetujui data ini. Aksi ini tidak dapat dikembalikan. Apakah anda ingin melanjutkan?',
	'verify-confirmation' => 'Konfirmasi Verifikasi|Anda akan memverifikasi data ini. Aksi ini tidak dapat dikembalikan. Apakah anda ingin melanjutkan?',

	'reset-invoice' => 'Reset Invoice|Anda akan me-reset invoice ini menjadi pending. Aksi ini tidak dapat dikembalikan. Apakah anda ingin melanjutkan?',
];