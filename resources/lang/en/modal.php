<?php

return [
	'delete-confirmation' => 'Delete Confirmation|You are about to delete a data from database. This action cannot be undone. Do you still want to continue?',

	'process-order-confirmation' => 'Process Order Confirmation|You are about to process order to enter next step. This action cannot be undone. Do you still want to continue?',
	'archive-confirmation' => 'Archive Confirmation|You are about to archive a data. This action cannot be undone. Do you still want to continue?',
	'approve-confirmation' => 'Approve Confirmation|You are about to approve this data. This action cannot be undone. Do you still want to continue?',
	'verify-confirmation' => 'Verify Confirmation|You are about to verify this data. This action cannot be undone. Do you still want to continue?',

	'reset-invoice' => 'Reset Invoice|You are about to reset this invoice to pending status. This action cannot be undone. Do you still want to continue?',
];