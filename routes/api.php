<?php

use App\Services\ConfigurationService;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Kavist\RajaOngkir\Facades\RajaOngkir;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('raja-ongkir/province', function () {
    $results = Request::query('q')
        ? RajaOngkir::provinsi()->search(Request::query('q'))->get()
        : RajaOngkir::provinsi()->all();

    return Response::json([
        'results' => Collection::make($results)->transform(function ($item) {
            return [
                'id' => $item['province_id'] . '_' . Str::slug($item['province']),
                'text' => $item['province'],
            ];
        }),
    ]);
})->name('api.raja-ongkir.province');

Route::get('raja-ongkir/city', function () {
    $results = Request::query('q')
        ? RajaOngkir::kota()->dariProvinsi(Request::query('province'))->search(Request::query('q'))->get()
        : RajaOngkir::kota()->dariProvinsi(Request::query('province'))->get();

    return Response::json([
        'results' => Collection::make($results)->transform(function ($item) {
            return [
                'id' => $item['city_id'] . '_' . Str::slug($item['type'] . ' ' . $item['city_name']),
                'text' => $item['type'] . ' ' . $item['city_name']
            ];
        })
    ]);
})->name('api.raja-ongkir.city');

Route::post('raja-ongkir/cost', function () {
    $validator = Validator::make(Request::all(), [
        'destination' => 'required',
        'weight' => 'required',
    ]);

    if ($validator->fails()) {
        return Response::json($validator->errors(), 400);
    }

    $results = [];
    $origin = ConfigurationService::config('warehouse_city');
    foreach(['jne', 'pos', 'tiki'] as $courier) {
        $results[] = RajaOngkir::ongkosKirim([
            'origin' => explode('_', $origin)[0],
            'destination' => Request::get('destination'),
            'weight' => Request::get('weight'),
            'courier' => $courier,
        ])->get()[0];
    }
    
    return Response::json(['results' => $results]);
})->name('api.raja-ongkir.cost');
