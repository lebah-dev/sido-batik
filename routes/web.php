<?php

use App\Http\Controllers\ConfigurationController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\MarketingMaterialController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\OrderInvoiceController;
use App\Http\Controllers\PlaceOrderController;
use App\Http\Controllers\PricingController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\StorageController;
use App\Http\Controllers\TrackOrderController;
use App\Http\Controllers\TransferMarginController;
use App\Http\Controllers\TransferMethodController;
use App\Http\Controllers\UserManagementController;
use App\Http\Middleware\AdminMiddleware;
use App\Http\Middleware\MustHaveProfile;
use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/track-order')->name('home');
Route::get('/place-order', [PlaceOrderController::class, 'create'])->name('place-order');
Route::post('/place-order', [PlaceOrderController::class, 'store'])->name('place-order');
Route::get('/track-order', [TrackOrderController::class, 'index'])->name('track-order');

Route::post('/order-managements/{order}/images', [OrderController::class, 'storeImage'])->name('order-managements.store-image');
Route::delete('/order-managements/{order}/images/{image}', [OrderController::class, 'deleteImage'])->name('order-managements.delete-image');
Route::put('/order-managements/{order}/designs/{design}/approve', [OrderController::class, 'approveDesign'])->name('order-managements.approve-design');
Route::post('/order-managements/{order}/designs/{design}/revise', [OrderController::class, 'reviseDesign'])->name('order-managements.revise-design');
Route::post('/order-managements/{order}/designs/{design}/revise/{revision}/attachment', [OrderController::class, 'storeReviseAttachment'])->name('order-managements.revise-design.store-attachment');

Route::put('/order-managements/{order}/fix-order', [OrderController::class, 'fixOrder'])->name('order-managements.fix-order');
Route::put('/order-managements/{order}/shipping-data', [OrderController::class, 'saveShippingData'])->name('order-managements.shipping-data');
Route::post('/order-managements/{order}/upload-delivery-receipt', [OrderController::class, 'uploadDeliveryReceipt'])->name('order-managements.upload-delivery-receipt');

Route::get('/order-invoice/{invoice:number}', [OrderInvoiceController::class, 'show'])->name('order-invoice.show');
Route::post('/order-invoice/{invoice:number}/paid', [OrderInvoiceController::class, 'paid'])->name('order-invoice.paid');
Route::put('/order-invoice/{invoice:number}/reset', [OrderInvoiceController::class, 'reset'])->name('order-invoice.reset');
Route::get('/order-invoice/{invoice:number}/print', [OrderInvoiceController::class, 'print'])->name('order-invoice.print');

Route::middleware([Authenticate::class])->group(function () {
    ////////////// DASHBOARD ///////////////
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard')->middleware(MustHaveProfile::class);

    Route::get('/profile', [ProfileController::class, 'index'])->name('profile');
    Route::put('/profile', [ProfileController::class, 'updateAccount'])->name('profile.update-account');
    Route::post('/profile', [ProfileController::class, 'updateProfile'])->name('profile.update-profile');

    ////////////// USER MANAGEMENTS ///////////////
    Route::middleware([AdminMiddleware::class])->group(function () {
        Route::get('/configurations', [ConfigurationController::class, 'index'])->name('configurations');
        Route::put('/configurations', [ConfigurationController::class, 'update'])->name('configurations');
        Route::delete('/configurations', [ConfigurationController::class, 'reset'])->name('configurations');

        Route::post('/transfer-methods', [TransferMethodController::class, 'store'])->name('transfer-methods.store');
        Route::delete('/transfer-methods/{bank}', [TransferMethodController::class, 'delete'])->name('transfer-methods.delete');

        Route::get('/user-managements', [UserManagementController::class, 'index'])->name('user-managements.index');
        Route::get('/user-managements/create', [UserManagementController::class, 'create'])->name('user-managements.create');
        Route::post('/user-managements/store', [UserManagementController::class, 'store'])->name('user-managements.store');
        Route::get('/user-managements/{user}', [UserManagementController::class, 'show'])->name('user-managements.show');
        Route::post('/user-managements/{user}/profile', [UserManagementController::class, 'updateProfile'])->name('user-managements.update-profile');
        Route::put('/user-managements/{user}/verify', [UserManagementController::class, 'verify'])->name('user-managements.verify');
        Route::get('/user-managements/{user}/edit', [UserManagementController::class, 'edit'])->name('user-managements.edit');
        Route::put('/user-managements/{user}', [UserManagementController::class, 'update'])->name('user-managements.update');
        Route::delete('/user-managements/{user}', [UserManagementController::class, 'delete'])->name('user-managements.delete');

        Route::get('/pricings', [PricingController::class, 'index'])->name('pricings.index');
        Route::get('/pricings/create', [PricingController::class, 'create'])->name('pricings.create');
        Route::post('/pricings', [PricingController::class, 'store'])->name('pricings.store');
        Route::get('/pricings/{pricing}', [PricingController::class, 'edit'])->name('pricings.edit');
        Route::delete('/pricings/{pricing}', [PricingController::class, 'destroy'])->name('pricings.destroy');
    });

    Route::put('/user-managements/{user}/bank-account', [UserManagementController::class, 'updateBankAccount'])->name('user-managements.bank-account');

    ////////////// ORDER MANAGEMENTS ///////////////
    Route::get('/order-managements', [OrderController::class, 'index'])->name('order-managements.index');
    Route::get('/order-managements/create', [OrderController::class, 'create'])->name('order-managements.create');
    Route::post('/order-managements', [OrderController::class, 'store'])->name('order-managements.store');

    Route::get('/order-managements/{order}', [OrderController::class, 'show'])->name('order-managements.show');

    Route::get('/order-managements/{order}/edit', [OrderController::class, 'edit'])->name('order-managements.edit');
    Route::put('/order-managements/{order}/update', [OrderController::class, 'update'])->name('order-managements.update');
    
    Route::delete('/order-managements/{order}/archive', [OrderController::class, 'archive'])->name('order-managements.archive');
    Route::put('/order-managements/{order}/process', [OrderController::class, 'process'])->name('order-managements.process');

    Route::post('/order-managements/{order}/designs', [OrderController::class, 'storeDesign'])->name('order-managements.store-design');
    
    Route::put('/order-managements/{order}/shipping-code', [OrderController::class, 'saveShippingCode'])->name('order-managements.shipping-code');
    Route::put('/order-managements/{order}/claim-margin', [OrderController::class, 'claimMargin'])->name('order-managements.claim-margin');

    ////////////// ORDER INVOICES ///////////////
    Route::post('/order-invoice/{order}', [OrderInvoiceController::class, 'create'])->name('order-invoice.create');

    ////////////// TRANSFER MARGIN ///////////////
    Route::get('/transfer-margin', [TransferMarginController::class, 'index'])->name('transfer-margin.index');

    ////////////// MARGIN CLAIMS ///////////////
    Route::get('/margin-claims', [TransferMarginController::class, 'reseller'])->name('margin-claims.index');
    
    ////////////// MARKETING MATERIALS ///////////////
    Route::resource('marketing-materials', MarketingMaterialController::class);
});

Route::get('/storage-get/{path}', [StorageController::class, 'get'])->name('storage-get');
Route::get('/storage-download/{path}', [StorageController::class, 'download'])->name('storage-download');