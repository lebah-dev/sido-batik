# Sido Batik

## Installation

- clone this repo
- `composer install`
- `cp .env.example .env`
- create db
- update `.env` file
- `php artisan migrate`
- `php artisan key:generate`
- `php artisan db:seed --class DefaultUserSeeder`
- `php artisan serve`