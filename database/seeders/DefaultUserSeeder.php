<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DefaultUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::query()->updateOrCreate([
            'email' => 'admin@admin.com',
        ], [
            'role' => User::ROLE_ADMIN,
            'name' => 'Admin Sido Batik',
            'password' => bcrypt('admin'),
            'email_verified_at' => now(),
        ]);
    }
}
