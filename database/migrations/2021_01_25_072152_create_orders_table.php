<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('number');
            $table->foreignId('reseller_id')->constrained('users')->onDelete('cascade');
            $table->string('status');
            $table->string('customer');
            $table->text('address');
            $table->string('wa_number');
            $table->longTExt('request_description');
            $table->integer('quantity')->nullable();
            $table->boolean('is_design_claimed')->default(false);
            $table->text('shipping_address')->nullable();
            $table->float('total_price')->nullable();
            $table->string('shipping_code')->nullable();
            $table->timestamp('reseller_claimed_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
