<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRevisionIdToOrderDesignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_designs', function (Blueprint $table) {
            $table->foreignId('revision_id')
                ->nullable()
                ->constrained('order_design_revisions')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_designs', function (Blueprint $table) {
            $table->dropConstrainedForeignId('revision_id');
        });
    }
}
