<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShippingDataToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('receiver_name')->nullable()->after('is_design_claimed');
            $table->string('receiver_phone')->nullable()->after('receiver_name');
            $table->string('shipping_province')->nullable()->after('receiver_phone');
            $table->string('shipping_city')->nullable()->after('shipping_province');
            $table->string('courier_name')->nullable()->after('shipping_address');
            $table->string('courier_service')->nullable()->after('courier_name');
            $table->string('courier_estimated_delivery')->nullable()->after('courier_service');
            $table->decimal('delivery_fee', 13, 2)->nullable()->after('courier_estimated_delivery');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('delivery_fee');
            $table->dropColumn('courier_estimated_delivery');
            $table->dropColumn('courier_service');
            $table->dropColumn('courier_name');
            $table->dropColumn('shipping_city');
            $table->dropColumn('shipping_province');
            $table->dropColumn('receiver_phone');
            $table->dropColumn('receiver_name');
        });
    }
}
