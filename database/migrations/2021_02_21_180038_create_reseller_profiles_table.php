<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResellerProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reseller_profiles', function (Blueprint $table) {
            $table->foreignId('id')->key()
                ->constrained('users')
                ->onDelete('cascade');
            $table->string('full_name');
            $table->string('identity_number');
            $table->string('wa_number');
            $table->string('facebook_account');
            $table->text('portrait_photo_path');
            $table->text('identity_scan_path');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reseller_profiles');
    }
}
